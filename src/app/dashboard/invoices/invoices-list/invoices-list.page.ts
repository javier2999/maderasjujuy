import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { InvoicesService } from "../invoices.service";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";

@Component({
  selector: 'app-invoices-list',
  templateUrl: './invoices-list.page.html',
  styleUrls: ['./invoices-list.page.scss'],
})
export class InvoicesPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  invoices: any;
  invoicesSinFiltro: any;
  isLoading = false;
  searchInvoice: Object = { invoices: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  invoice: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: InvoicesService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService
  ) {
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });

    this.getInvoices();
    
    this.hide_sm = false;
  }

  ngOnInit() {
    //this.getUsers();
  }

  getInvoices() {
    this.searchInvoice["invoice"] = this.invoice;
    this.searchInvoice["offset"] = 0;
    this.searchInvoice["limit"] = this.infiniteRow;
    this.isLoading = true;
    /*this.service                                REVISAR
      .getProvidersGroup(this.searchProvider)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.providers = this.dataStatus.providers;
          console.log(this.providers);
          this.providersSinFiltro = this.dataStatus;
          //console.log(this.users);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });*/
    this.isLoading = false;
  }

  /*async edit(providerIndex: any) {
    let provider = this.providers[providerIndex];
    this.editModal = await this.modalController.create({
      component: ProvidersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        providerPar: provider,
        idx: providerIndex,
        onSave: this.saveProviderEdited,
        onCancel: this.cancelProviderEdition
      }
    });
    return await this.editModal.present();
  }

  async addProvider() {
    let providerPar = {
      id: null,
      cuit: "",
      company_name: "",
      ivacategid: "",
      phone: "",
      active: 1
    };
    this.editModal = await this.modalController.create({
      component: ProvidersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        providerPar: providerPar,
        idx: null,
        onSave: this.addNewProvider,
        onCancel: this.cancelProviderEdition
      }
    });
    return await this.editModal.present();
  }*/

  downloadExcel() {
    this.excelService.exportAsExcelFile(this.invoices, "invoices");
  }

  /*saveProviderEdited = (providerInfo: any, providerIndex: any) => {
    this.providers[providerIndex] = providerInfo;
    this.editModal.dismiss();
  };

  addNewProvider = (providerInfo: any, providerIndex: any) => {
    this.providers.push(providerInfo);
    this.editModal.dismiss();
  };

  cancelProviderEdition = () => {
    this.editModal.dismiss();
  };*/

  async delete(invoiceIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Factura",
      message: "¿Confirma que quiere borrar la factura?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let invoice = this.invoices[invoiceIndex];
            let par = { id: invoice.id, active: 0 };

            this.service
              .deleteInvoice(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getInvoices();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }

  initializeItems() {
    this.invoices = this.invoicesSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getUsers();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.invoices = this.invoices.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.invoice.length == 0) {
        this.getInvoices();
      }
    }
  }
}



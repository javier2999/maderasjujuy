import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { ProvidersService } from "../providers.service";
import { ProvidersAddComponent } from "../providers-add/providers-add.component";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";

@Component({
  selector: 'app-providers',
  templateUrl: './providers.page.html',
  styleUrls: ['./providers.page.scss'],
})
export class ProvidersPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  providers: any;
  providersSinFiltro: any;
  isLoading = false;
  searchProvider: Object = { providers: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  provider: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: ProvidersService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService
  ) {
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });

    this.getProviders();
    
    this.hide_sm = false;
  }

  ngOnInit() {
    //this.getUsers();
  }

  getProviders() {
    this.searchProvider["provider"] = this.provider;
    this.searchProvider["offset"] = 0;
    this.searchProvider["limit"] = this.infiniteRow;
    this.isLoading = true;
    /*this.service                                REVISAR
      .getProvidersGroup(this.searchProvider)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.providers = this.dataStatus.providers;
          console.log(this.providers);
          this.providersSinFiltro = this.dataStatus;
          //console.log(this.users);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });*/
    this.isLoading = false;
  }

  async edit(providerIndex: any) {
    let provider = this.providers[providerIndex];
    this.editModal = await this.modalController.create({
      component: ProvidersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        providerPar: provider,
        idx: providerIndex,
        onSave: this.saveProviderEdited,
        onCancel: this.cancelProviderEdition
      }
    });
    return await this.editModal.present();
  }

  async addProvider() {
    let providerPar = {
      id: null,
      cuit: "",
      company_name: "",
      ivacategid: "",
      phone: "",
      active: 1
    };
    this.editModal = await this.modalController.create({
      component: ProvidersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        providerPar: providerPar,
        idx: null,
        onSave: this.addNewProvider,
        onCancel: this.cancelProviderEdition
      }
    });
    return await this.editModal.present();
  }

  downloadExcel() {
    this.excelService.exportAsExcelFile(this.providers, "providers");
  }

  saveProviderEdited = (providerInfo: any, providerIndex: any) => {
    this.providers[providerIndex] = providerInfo;
    this.editModal.dismiss();
  };

  addNewProvider = (providerInfo: any, providerIndex: any) => {
    this.providers.push(providerInfo);
    this.editModal.dismiss();
  };

  cancelProviderEdition = () => {
    this.editModal.dismiss();
  };

  async delete(providerIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Proveedor",
      message: "¿Confirma que quiere borrar el proveedor?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let provider = this.providers[providerIndex];
            let par = { id: provider.id, active: 0 };

            this.service
              .deleteProvider(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getProviders();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }

  initializeItems() {
    this.providers = this.providersSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getUsers();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.providers = this.providers.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.provider.length == 0) {
        this.getProviders();
      }
    }
  }
}


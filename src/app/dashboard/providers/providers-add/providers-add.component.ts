import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import { NavParams, LoadingController, ToastController } from "@ionic/angular";
import { ProvidersService } from "../providers.service";

/* import { ApiServiceProvider } from '../../providers/api-service/api-service'; */
import { LoginPage } from "../../../authentication/login/login.page";
import { ProvidersPage } from "../providers-list/providers.page";

/**
 * Generated class for the UsersAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'app-providers-add',
  templateUrl: './providers-add.component.html',
  styleUrls: ['./providers-add.component.scss'],
})
export class ProvidersAddComponent {
  @Input() providerPar: any;
  @Input() idx: any;
  @Input() onSave: any;
  @Input() onCancel: any;

  loader: any;
  dataStatus: any;
  hide_sm: any;
  isBrowser: Boolean = false;
  inputGroup: number = 0;
  messaje: string = "";
  class: string = "alert d-none";
  provider: any = {
    id: null,
    cuit: "",
    company_name: "",
    ivacategid: "",
    phone: "",
    active: 1
  };
  providerForm: FormGroup;
  validation_messages: any;

  constructor(
    private fb: FormBuilder,
    private service: ProvidersService,
    private navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {
    /*     if (this.platform.is('mobileweb') || this.platform.is('core')) {
          this.isBrowser = true;
      }
      this.hide_sm = platform.width() < 576; */
    this.provider = this.navParams.get("providerPar");
    this.onSave = this.navParams.get("onSave");
    this.onCancel = this.navParams.get("onCancel");
    this.idx = this.navParams.get("idx");
    /*this.inputGroup =
      typeof this.provider["provider-group"] !== "undefined" &&
      this.provider["provider-group"] !== null
        ? this.provider["provider-group"][0].group[0].id
        : 1;*/
    this.createForm();
    //this.getGroups();
  }

  /* ionViewDidLoad() {
      this.getGroups();
    } */

  /* clear() {
      this.user = {
        id: 0,
        dni: 0,
        last_name: "",
        first_name: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id : 1
    };
    this.messaje = "";
    this.class= 'alert d-none';
    this.userForm.reset({
        id: 0,
        dni: 0,
        last_name: "",
        first_name: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id: 1
    });
  } */

  createForm() {
    this.validation_messages = {
      cuit: [
        { type: "required", message: "El CUIT es obligatorio." },
        {
          type: "pattern",
          message:
            "El CUIT debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      company_name: [
        {
          type: "minlength",
          message: "El nombre debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El nombre es obligatorio." }
      ],
      ivacategid: [
        { type: "required", message: "La categoria es obligatoria." }
      ],
      phone: [
        {
          type: "pattern",
          message: "El telefono no coincide con un formato válido."
        },
        { type: "required", message: "El telefono es obligatorio." },
        { type: "validEmail", message: "El telefono ya existe o no es válido." }
      ],
      
    };
    this.providerForm = this.fb.group({
      id: this.provider.id,
      cuit: new FormControl(
        this.provider.cuit,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,9}$")
        ])
      ),
      company_name: new FormControl(
        this.provider.company_name,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      ivacategid: new FormControl(
        this.provider.ivacategid,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      phone: this.provider.phone,
      
      active: this.provider.active
    });
  }

  save() {
    //this.presentToast("Guardandoo...", "alert alert-secondary");
    //this.user.id = null;
    //this.validate_sys_user((response) => response.JSON());
    let provider = this.providerForm.value;

    if (typeof provider.id !== "undefined" && provider.id !== null) {
      this.service
        .updateProvider(provider)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            //Login
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Proveedor Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addProvider(provider)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el provider", "danger");
          } else {
            provider.id = this.dataStatus.insertId;
            this.presentToast("Proveedor Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(provider, this.idx);
  }

  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  cancel() {
    //this.navCtrl.setRoot(UsuariosPage, {}, { animate: true, direction: 'forward' });
    //this.router.navigate(['dashboard/users']);
    this.onCancel();
  }
}



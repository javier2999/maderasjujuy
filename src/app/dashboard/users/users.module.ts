import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { UsersPage } from "./users-list/users.page";
import {
  UsersProfileComponent,
  CroppImagePage
} from "./users-profile/users-profile.component";
import { UsersAddComponent } from "./users-add/users-add.component";
import { ImageCropperModule } from "ngx-image-cropper";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
//import { DriversComponent } from "./users-drivers/drivers/drivers.component";

const routes: Routes = [
  {
    path: "",
    component: UsersPage
  },
  {
    path: "profile",
    component: UsersProfileComponent
  },
  {
    path: "add",
    component: UsersAddComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes),
    NgxDatatableModule
  ],
  declarations: [
    UsersPage,
    UsersProfileComponent,
    UsersAddComponent,
    CroppImagePage,
  ],
  entryComponents: [CroppImagePage]
})
export class UsersPageModule {}

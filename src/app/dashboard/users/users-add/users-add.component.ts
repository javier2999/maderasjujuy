import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import { NavParams, LoadingController, ToastController } from "@ionic/angular";
import { UsersService } from "../users.service";

/* import { ApiServiceProvider } from '../../providers/api-service/api-service'; */
import { LoginPage } from "../../../authentication/login/login.page";
import { UsersPage } from "../users-list/users.page";

/**
 * Generated class for the UsersAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-users-add",
  templateUrl: "users-add.component.html"
})
export class UsersAddComponent {
  @Input() userPar: any;
  @Input() idx: any;
  @Input() onSave: any;
  @Input() onCancel: any;

  loader: any;
  dataStatus: any;
  hide_sm: any;
  isBrowser: Boolean = false;
  inputGroup: number = 0;
  messaje: string = "";
  class: string = "alert d-none";
  groups: any;
  selectedGroup: number;
  user: any = {
    id: null,
    nick: "",
    email: "",
    password: "",
    active: 0,
    is_admin: 0,
    sys_group_id: 0,
    encryptPassword: false
  };
  userForm: FormGroup;
  validation_messages: any;

  constructor(
    private fb: FormBuilder,
    private service: UsersService,
    private navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {
    /*     if (this.platform.is('mobileweb') || this.platform.is('core')) {
          this.isBrowser = true;
      }
      this.hide_sm = platform.width() < 576; */
    this.user = this.navParams.get("userPar");
    this.onSave = this.navParams.get("onSave");
    this.onCancel = this.navParams.get("onCancel");
    this.idx = this.navParams.get("idx");
    this.inputGroup =
      typeof this.user["user-group"] !== "undefined" &&
      this.user["user-group"] !== null
        ? this.user["user-group"][0].group[0].id
        : 1;
    this.createForm();
    this.getGroups();
  }

  /* ionViewDidLoad() {
      this.getGroups();
    } */

  /* clear() {
      this.user = {
        id: 0,
        dni: 0,
        last_name: "",
        nick: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id : 1
    };
    this.messaje = "";
    this.class= 'alert d-none';
    this.userForm.reset({
        id: 0,
        dni: 0,
        last_name: "",
        nick: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id: 1
    });
  } */

  createForm() {
    this.validation_messages = {
      dni: [
        { type: "required", message: "El DNI es obligatorio." },
        {
          type: "pattern",
          message:
            "El DNI debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      nick: [
        {
          type: "minlength",
          message: "El nombre debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El nombre es obligatorio." }
      ],
      email: [
        {
          type: "pattern",
          message: "El email no coincide con un formato válido."
        },
        { type: "required", message: "El email es obligatorio." },
        { type: "validEmail", message: "El email ya existe o no es válido." }
      ],
      username: [
        { type: "required", message: "Username is required." },
        {
          type: "minlength",
          message: "La contraseña debe tener mas de 5 caracteres."
        },
        {
          type: "maxlength",
          message: "La contraseña debe tener menos de 25 caracteres."
        },
        {
          type: "pattern",
          message:
            "La contraseña debe estar compuesta solo por numeros y letras."
        }
      ]
    };
    this.userForm = this.fb.group({
      id: this.user.id,
      dni: new FormControl(
        this.user.dni,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,9}$")
        ])
      ),
      nick: new FormControl(
        this.user.nick,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      phone: this.user.phone,
      email: new FormControl(
        this.user.email,
        Validators.compose([
          UsersService.validEmail,
          Validators.required,
          Validators.pattern("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")
        ])
      ),
      password: new FormControl(
        this.user.password,
        Validators.compose([
          Validators.maxLength(25),
          Validators.minLength(5),
          Validators.pattern("^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$"),
          Validators.required
        ])
      ),
      active: this.user.active,
      is_admin: this.user.is_admin,
      sys_group_id: this.inputGroup,
      groupid : this.inputGroup
    });
  }

  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .getGroups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }

  setGroup(groupId: number) {
    if (groupId == 3) {
      this.user.is_admin = 1;
    }
    this.user.sys_group_id = groupId;
    this.user.sys_group_id = 1;
    this.selectedGroup = groupId;
  }

  save() {
    //this.presentToast("Guardandoo...", "alert alert-secondary");
    //this.user.id = null;
    //this.validate_sys_user((response) => response.JSON());
    let user = this.userForm.value;

    if (this.user.password !== this.userForm.value.password) {
      user.encryptPassword = true;
    }
    if (typeof user.id !== "undefined" && user.id !== null) {
      this.service
        .updateUser(user)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            //Login
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Usuario Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addUser(user)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el usuario", "danger");
          } else {
            console.log(data);
            user.id = this.dataStatus.insertId;
            this.presentToast("Usuario Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(user, this.idx);
  }

  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  cancel() {
    //this.navCtrl.setRoot(UsuariosPage, {}, { animate: true, direction: 'forward' });
    //this.router.navigate(['dashboard/users']);
    this.onCancel();
  }
}

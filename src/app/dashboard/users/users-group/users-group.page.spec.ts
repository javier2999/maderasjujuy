import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersGroupPage } from './users-group.page';

describe('UsersGroupPage', () => {
  let component: UsersGroupPage;
  let fixture: ComponentFixture<UsersGroupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersGroupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersGroupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

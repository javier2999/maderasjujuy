import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: "root"
})
export class UsersService {
  baseURL: String;
  headers: any;
  tableStyle = 'bootstrap';

  constructor(public http: HttpClient, private storage: Storage) {}

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });

    
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  user: any;
  resultUser: any;
  profileImg: any;
  getUser() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "user", { headers: this.headers })
          .subscribe(data => {
            this.resultUser = data;
            if (this.resultUser.length > 0) {
              this.user = this.resultUser[0];
              /*this.getProfileImg().then(() => {
                this.user.pic = this.profileImg[0].pic;
                resolve(this.user);
              });*/
              this.user.pic = "";
              resolve(this.user);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultUser);
              });
            }
          });
      });
    });
  }

  getProfileImg() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(
            API_REFLECT_URL +
              `picture?filters=sys_user_id:${this.user.id}&sort=id&order=DESC&limit=1`,
            { headers: this.headers }
          )
          .subscribe(data => {
            this.profileImg = data;
            if (
              typeof this.profileImg.status !== "undefined" &&
              this.profileImg.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.profileImg);
              });
            } else {
              resolve(this.profileImg);
            }
          });
      });
    });
  }

  responseSetImg: any;
  setProfileImg(_base64Img) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(
            API_REFLECT_URL + `/picture`,
            { pic: _base64Img, sys_user_id: this.user.id },
            { headers: this.headers }
          )
          .subscribe(data => {
            this.responseSetImg = data;
            if (
              typeof this.responseSetImg.status !== "undefined" &&
              this.responseSetImg.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.responseSetImg);
              });
            } else {
              resolve(this.responseSetImg);
            }
          });
      });
    });
  }

  changePasswordResp: any;
  changePassword(oldPassword, newPassword, repPassword) {
    let encryptPassword = true;
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .put(
            API_AUTH_URL + `change-password`,
            { oldPassword, newPassword, repPassword, encryptPassword },
            { headers: this.headers }
          )
          .subscribe(data => {
            this.changePasswordResp = data;
            if (
              typeof this.changePasswordResp.status !== "undefined" &&
              this.changePasswordResp.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.changePasswordResp);
              });
            } else {
              resolve(this.changePasswordResp);
            }
          });
      });
    });
  }

  // -------------------------- Local DB ------------------------- //
  usuarios: any;
  getUsuarios(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `user/user-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.usuarios = data;
            if (
              typeof this.usuarios.status !== "undefined" &&
              this.usuarios.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.usuarios);
              });
            } else {
              resolve(this.usuarios);
            }
          });
      });
    });
  }

  groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}get_groups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }

  getUsers() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `user`, { headers: this.headers })
          .subscribe(data => {
            this.usuarios = data;
            if (
              typeof this.usuarios.status !== "undefined" &&
              this.usuarios.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.usuarios);
              });
            } else {
              resolve(this.usuarios);
            }
          });
      });
    });
  }

  //acá hace la llamada al grid de user-list
  getUsersGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}get_users.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.usuarios = data;
            if (
              typeof this.usuarios.status !== "undefined" &&
              this.usuarios.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.usuarios);
              });
            } else {
              resolve(this.usuarios);
            }
          });
      });
    });
  }

  getAUser(userId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `user?filters=a.id:${userId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.user = data;
            if (
              typeof this.user.status !== "undefined" &&
              this.user.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.user);
              });
            } else {
              resolve(this.user);
            }
          });
      });
    });
  }

  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }

  addUser(user: any): any {

    let url = API_REFLECT_URL + "create_user.php";

    let dataUser = JSON.stringify(user);
    console.log(user);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataUser, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "user_id": response.insertId,
                "newPassword": user.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.user.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  /*   deleteUser(user: any): any {
    let url = this.baseURL + 'reflect/user/';
    let data = '{"id": '+user.id+'}';
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    return this.http.delete(url, httpOptions);
  } */

  deleteUser(user: any): any {
    let url = API_REFLECT_URL + "delete_user.php";
    console.log(user);
    user.active = 0;
    let data = user
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateUser(user: any) {
    let url = API_REFLECT_URL + "update_user.php";
    let data = JSON.stringify(user);
    let response: any;
    console.log(user);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }

  changeAPassword(password: any): any {
    let url = API_REFLECT_URL + "user/set_password";
    let body = password;
    return this.http.put(url, body, { headers: this.headers });
  }

  verifyMAil(mail: string): any {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}get_users.php`, {headers: this.headers}
          )
          .subscribe(data => {
            this.user = data;
            if (
              typeof this.user.status !== "undefined" &&
              this.user.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.user);
              });
            } else {
              resolve(this.user);
            }
          });
      });
    });
  }

  static validEmail(fc: FormControl) {
    let mail = fc.value.toLowerCase();

    if (
      fc.value.toLowerCase() === "abc123@gmail.com" ||
      fc.value.toLowerCase() === "123abc@gmail.com"
    ) {
      return { validEmail: true };
    } else {
      return null;
    }
  }
}

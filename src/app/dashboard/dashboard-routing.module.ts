import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
      path:"",
      redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: "./home/home.module#HomePageModule"
  },/*
  {
    path: 'client',
    loadChildren: () => import('./clients/client-list/client.module').then( m => m.ClientPageModule)
  },
  {
    path: "users",
    loadChildren: "./users/users.module#UsersPageModule"
  },
  {
    path: "groups",
    loadChildren: "./groups/groups.module#GroupsModule"
  },
  {
    path: "clients",
    loadChildren: "./clients/clients.module#ClientsPageModule"
  },
  {
    path: "cars",
    loadChildren: "./cars/cars.module#CarsModule"
  },
  {
    path: "about",
    loadChildren: "./about/about.module#AboutPageModule"
  },
  { 
    path: 'drivers',
    loadChildren: './drivers/drivers.module#DriversPageModule'
  },
  { 
    path: 'travels',
    loadChildren: './travels/travels.module#TravelsPageModule'
  },
  {
    path: 'map',
    loadChildren: './map/map.module#MapPageModule'
  }*/
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./clients/clients.module').then( m => m.ClientsPageModule)
  },
  {
    path: 'sells',
    loadChildren: () => import('./sells/sells.module').then( m => m.SellsPageModule)
  },
  {
    path: 'providers',
    loadChildren: () => import('./providers/providers.module').then( m => m.ProvidersPageModule)
  },
  {
<<<<<<< HEAD
=======
    path: 'users-group',
    loadChildren: () => import('./users/users-group/users-group.module').then( m => m.UsersGroupPageModule)
  },
  {
    path: 'users-employees',
    loadChildren: () => import('./users/users-employees/users-employees.module').then( m => m.UsersEmployeesPageModule)
  },
  {
>>>>>>> 44c57e5d222dc2e258bf779484afe3814195a4e6
    path: 'categories',
    loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesPageModule)
  },
  {
    path: 'profiles',
    loadChildren: () => import('./profiles/profiles.module').then( m => m.ProfilespageModule)
  },
  {
    path: 'employee',
    loadChildren: () => import('./employee/employee.module').then( m => m.EmployeepageModule)
  },
  {
    path: 'paymentmethod',
    loadChildren: () => import('./paymentmethod/paymentmethod.module').then( m => m.PaymentmethodpageModule)
  },
  
  {
    path: 'typesofcustomer',
    loadChildren: () => import('./typesofcustomer/typesofcustomer.module').then( m => m.TypesofcustomerpageModule)
  },
  {
    path: 'productstypes',
    loadChildren: () => import('./ProductsTypes/products-types.module').then( m => m.ProductsTypespageModule)
  },
  {
    path: 'groups',
    loadChildren: () => import('./groups/groups.module').then( m => m.GroupspageModule)
  },
<<<<<<< HEAD

=======
  {
    path: 'invoices',
    loadChildren: () => import('./invoices/invoices.module').then( m => m.InvoicesPageModule)
  },
  {
    path: 'detailinvoices',
    loadChildren: () => import('./detailinvoices/detailinvoices.module').then( m => m.DetailinvoicesModule)
  },
  {
    path: 'cashregisters',
    loadChildren: () => import('./cashregisters/cashregisters.module').then( m => m.CashregistersModule)
  },
>>>>>>> 44c57e5d222dc2e258bf779484afe3814195a4e6
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DashboardRoutingModule {}

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { LoadingController, NavParams, ToastController } from '@ionic/angular';
import { ProductsTypesService } from '../products-types.service';

@Component({
  selector: 'app-products-types-add',
  templateUrl: './products-types-add.component.html',
  styleUrls: ['./products-types-add.component.scss'],
})
export class ProductsTypesAddComponent {
  @Input() producttypePar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;

  loader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  productype:any={
    id: null,
    description: "",
    gain:"",
    active: 0,

  
  };
  productypeForm:FormGroup;
  validation_messages:any;
  constructor(
    private fb:FormBuilder,
    private service:ProductsTypesService,
    private navParams:NavParams,
    public loadingController:LoadingController,
    public toastController:ToastController
  ) {
    this.productype=this.navParams.get("producttypePar");
    this.onSave=this.navParams.get("onSaver");
    this.onCancel=this.navParams.get("onCancel");
    this.idx=this.navParams.get("idx");
    this.inputGroup=
    typeof this.productype["producttype-group"] !== "undefined"&&
    this.productype["producttype-group"] !==null
    ? this.productype["producttype-group"][0].group[0].id
    :1;
    this.createForm();
    this.getGroups();
   }
  createForm() {
    this.validation_messages = {
      description: [
        {
          type: "minlength",
          message: "El tipo de prodcuto debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El tipo de prodcuto es obligatorio." }
      ],
     
      
    };
    this.productypeForm = this.fb.group({
      id: this.productype.id,
      description: new FormControl(
        this.productype.description,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),

      
      active: this.productype.active,
     
    });
  }
  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .get_Groups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.productype.is_admin = 1;
    }
    this.productype.sys_group_id = groupId;
    this.productype.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  save() {
     
    let productype = this.productypeForm.value;

    
    
    if (typeof productype.id !== "undefined" && productype.id !== null) {
      this.service
        .updateProducttype(productype)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Tipo Producto Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addProducttype(productype)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el Tipo de Producto", "danger");
          } else {
            console.log(data);
            productype.id = this.dataStatus.insertId;
            this.presentToast("Perfil Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(productype, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  } 
  cancel() {
   
    this.onCancel();
  }
}

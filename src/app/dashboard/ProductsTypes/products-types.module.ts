import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProductsTypesListPage } from './products-types-list/products-types-list.page';
import { ProductsTypesAddComponent } from './products-types-add/products-types-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


const routes:Routes=[
{
  path:"",
  component:ProductsTypesListPage
},{
  path:"add",
  component:ProductsTypesAddComponent
}

]

@NgModule({
  declarations: [
    ProductsTypesListPage,
    ProductsTypesAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ]
})
export class ProductsTypespageModule { }

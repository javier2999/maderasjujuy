import { TestBed } from '@angular/core/testing';

import { ProductsTypesService } from './products-types.service';

describe('ProductsTypesService', () => {
  let service: ProductsTypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductsTypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

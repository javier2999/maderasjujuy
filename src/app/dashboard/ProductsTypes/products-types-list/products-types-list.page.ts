import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ProductsTypesService } from '../products-types.service';
import { ExcelServiceProvider } from '../../../services/excelService/excel-service.service';
import { ScreenService } from '../../../app-screen.service';
import { ProductsTypesAddComponent } from '../products-types-add/products-types-add.component';


@Component({
  selector: 'app-products-types-list',
  templateUrl: './products-types-list.page.html',
  styleUrls: ['./products-types-list.page.scss'],
})
export class ProductsTypesListPage implements OnInit {
  @ViewChild("inputSearch",{static:true}) inputSearch;
  productstypes:any;
  productstypesSinFiltro:any;
  isLoading=false;
  searchProductstypes:Object={ producttype:"",offset:0, limit:200};
  global_vars:any;
  infiniteRow:number=100;
  hide_sm;
  producttype:string="";
  dataStatus:any;
  editModal:any;
  screen:any;
  constructor(
    private service:ProductsTypesService,
    public modalController:ModalController,
    public alertController:AlertController,
    public exelservice:ExcelServiceProvider,
    private screenService:ScreenService
  ) { 
    screenService.screen.subscribe(_screen =>{
      this.screen=_screen;
    });
    this.getProducttype();
    this.hide_sm=false;
  }

  ngOnInit() {
  }
  getProducttype(){

    this.searchProductstypes["producttype"] = this.producttype;
    this.searchProductstypes["offset"] = 0;
    this.searchProductstypes["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getproducttypeGroup(this.searchProductstypes)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        
        } else {
          this.producttype = this.dataStatus.profiles;
          console.log(this.producttype);
          this.productstypesSinFiltro = this.dataStatus;
          
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;

   }
   async edit(producttypeIndex: any) {
    let group = this.productstypes[producttypeIndex];
    this.editModal = await this.modalController.create({
      component: ProductsTypesAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        groupPar: group,
        idx: producttypeIndex,
        onSave: this.saveProducttypeEdited,
        onCancel: this.cancelProducttypeEdition
      }
    });
    return await this.editModal.present();
  }
  async addProducttype() {
    let producttypePar = {
      id: null,
      description: "",
      gain:"",
      active: 1,
      "producttype-group": null


    };
    this.editModal = await this.modalController.create({
      component: ProductsTypesAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        producttypePar: producttypePar,
        idx: null,
        onSave: this.addNewProducttype,
        onCancel: this.cancelProducttypeEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.exelservice.exportAsExcelFile(this.productstypes, "productstypes");
  }
  saveProducttypeEdited = (producttypeInfo: any,producttypeIndex: any) => {
    this.productstypes[producttypeIndex] = producttypeInfo;
    this.editModal.dismiss();
  };
  addNewProducttype = (producttypeInfo: any, producttypeIndex: any) => {
    this.productstypes.push(producttypeInfo);
    this.editModal.dismiss();
  };
  cancelProducttypeEdition = () => {
    this.editModal.dismiss();
  };
  async delete(producttypeIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Tipo de Producto",
      message: "¿Confirma que quiere borrar el Tipo de Producto",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let group = this.productstypes[producttypeIndex];
            let par = { id: group.id, active: 0 };

            this.service
              .deleteProducttype(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getProducttype();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }
  initializeItems() {
    this.productstypes = this.productstypesSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

   
    const val = ev.target.value;

    
    if (val && val.trim() != "") {
      this.productstypes = this.productstypes.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.productstypes.length == 0) {
        this.getProducttype();
      }
    }
  }

}

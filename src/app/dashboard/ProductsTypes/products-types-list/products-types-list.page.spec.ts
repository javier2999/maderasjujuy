import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductsTypesListPage } from './products-types-list.page';

describe('ProductsTypesListPage', () => {
  let component: ProductsTypesListPage;
  let fixture: ComponentFixture<ProductsTypesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsTypesListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsTypesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

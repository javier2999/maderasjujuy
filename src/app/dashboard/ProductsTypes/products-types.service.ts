import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

const API_AUTH_URL=`${environment.API_PATH}auth`;
const API_REGLECT_URL=`${environment.API_PATH}`;
@Injectable({
  providedIn: 'root'
})
export class ProductsTypesService {
  baseURL:string;
  headers:any;
  constructor(public http:HttpClient, private storage:Storage) { }
  set_headers(){
    return new Promise(resolve=>{
    this.storage.get("jtw").then(val=>{
      this.headers=new HttpHeaders({
        "content-type":"aplication/json",
        Accept:"aplication/json",
        Authorization:"Bearer"+val
      });
      resolve();
    });
    console.log(this.storage);
    })
  }
  close:any;
  closeSession(){
    return new Promise(resolve=>{
      this.set_headers().then(()=>{
        this.http
        .post(API_AUTH_URL+`logout`,{},{headers:this.headers})
        .subscribe(data=>{
          this.storage.remove("maderera-dash-jwt");
          this.close=data;
          resolve(this.close)
        });
      });
    });
  }
  producttype:any;
 resutProductsTypes:any;
 profileImg:any;
getProductType(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
    this.http
    .get(API_AUTH_URL+"producttype",{headers:this.headers})
    .subscribe(data=>{
      this.resutProductsTypes=data;
      if(this.resutProductsTypes.length>0){
      this.producttype=this.resutProductsTypes[0];
      this.producttype.pic="";
      resolve(this.producttype);}
      else{
        this.closeSession().then(()=>{
          resolve(this.resutProductsTypes);
          });
        }
      });
    });
  });
 };
 getProfileImg(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .get(
        API_REGLECT_URL + 
        `picture?filters=group:${this.producttype.id}&sort=id&order=Des&limit=1`,
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.profileImg=data;
        if(
          typeof this.profileImg.status !=="undefined"&&
          this.profileImg.status==false
        ){
          this.closeSession().then(()=>{
            resolve(this.profileImg);
          });
        }else{
          resolve(this.profileImg);
        }
      });
    });
  });
}
responseSetImg:any;
setProfileImg(_base64Img){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .post(
        API_REGLECT_URL+`/picture`,
        {pic: _base64Img, sys_group_id:this.producttype.id},
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.responseSetImg=data;
        if(
          typeof this.responseSetImg.status !=="undefined" &&
          this.responseSetImg.status==false
        ) {
          this.closeSession().then(()=>{
            resolve(this.responseSetImg);
          });
        } else{
          resolve(this.responseSetImg);
        }
      });
    });
  });
}
TiposProductos: any;
getTipoProducto(param) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(API_REGLECT_URL + `producttype/producttype-group/group`, {
          headers: this.headers
        })
        .subscribe(data => {
          this.TiposProductos = data;
          if (
            typeof this.TiposProductos.status !== "undefined" &&
            this.TiposProductos.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.TiposProductos);
            });
          } else {
            resolve(this.TiposProductos);
          }
        });
    });
  });
}
_groups: any;
  get_Groups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REGLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this._groups = data;
            if (
              typeof this._groups.status !== "undefined" &&
              this._groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this._groups);
              });
            } else {
              resolve(this._groups);
            }
          });
      });
    });
  }
   getProductsTypes() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `producttype`, { headers: this.headers })
          .subscribe(data => {
            this.TiposProductos = data;
            if (
              typeof this.TiposProductos.status !== "undefined" &&
              this.TiposProductos.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.TiposProductos);
              });
            } else {
              resolve(this.TiposProductos);
            }
          });
      });
    });
  }
  getproducttypeGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(`${API_AUTH_URL}getallproducttype.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.TiposProductos = data;
            if (
              typeof this.TiposProductos.status !== "undefined" &&
              this.TiposProductos.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.TiposProductos);
              });
            } else {
              resolve(this.TiposProductos);
            }
          });
      });
    });
  }
  getPRoducttype(producttypeId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + `producttype?filters=a.id:${producttypeId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.producttype = data;
            if (
              typeof this.producttype.status !== "undefined" &&
              this.producttype.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.producttype);
              });
            } else {
              resolve(this.producttype);
            }
          });
      });
    });
  }
  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  addProducttype(producttype: any): any {

    let url = API_REGLECT_URL + "create_producttype.php";

    let dataProducttype = JSON.stringify(producttype);
    console.log(producttype);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataProducttype, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
      
            resolve(response);
          }
        });
      });
    });
  }
  deleteProducttype(producttype: any): any {
    let url = API_REGLECT_URL + "delete_producttype.php";
    console.log(producttype);
    producttype.active = 0;
    let data = producttype
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  updateProducttype(producttype: any) {
    let url = API_REGLECT_URL + "update_producttype.php";
    let data = JSON.stringify(producttype);
    let response: any;
    console.log(producttype);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}

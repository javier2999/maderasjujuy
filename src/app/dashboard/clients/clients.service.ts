import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  baseURL: String;
  headers: any;

  constructor(public http: HttpClient, private storage: Storage) { }

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });    
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("maderera-dash-jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  client: any;
  resultClient: any;
  //profileImg: any;
  getClient() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "client", { headers: this.headers })
          .subscribe(data => {
            this.resultClient = data;
            if (this.resultClient.length > 0) {
              this.client = this.resultClient[0];
              /*this.getProfileImg().then(() => {
                this.user.pic = this.profileImg[0].pic;
                resolve(this.user);
              });*/
              this.client.pic = "";
              resolve(this.client);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultClient);
              });
            }
          });
      });
    });
  }

  // Faltan servicios de img

  // -------------------------- Local DB ------------------------- //
  clientes: any;
  getClientes(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `client/client-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.clientes = data;
            if (
              typeof this.clientes.status !== "undefined" &&
              this.clientes.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.clientes);
              });
            } else {
              resolve(this.clientes);
            }
          });
      });
    });
  }

  /*groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }*/

  getClients() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `client`, { headers: this.headers })
          .subscribe(data => {
            this.clientes = data;
            if (
              typeof this.clientes.status !== "undefined" &&
              this.clientes.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.clientes);
              });
            } else {
              resolve(this.clientes);
            }
          });
      });
    });
  }

  //acá hace la llamada al grid de user-list
  getClientsGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}getallclients.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.clientes = data;
            if (
              typeof this.clientes.status !== "undefined" &&
              this.clientes.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.clientes);
              });
            } else {
              resolve(this.clientes);
            }
          });
      });
    });
  }

  getAClient(clientId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `client?filters=a.id:${clientId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.client = data;
            if (
              typeof this.client.status !== "undefined" &&
              this.client.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.client);
              });
            } else {
              resolve(this.client);
            }
          });
      });
    });
  }

  /*permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }*/

  addClient(user: any): any {
    let url = API_REFLECT_URL + "create_client.php";
    let data = JSON.stringify(user);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "user_id": response.insertId,
                "newPassword": user.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.user.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  /*   deleteUser(user: any): any {
    let url = this.baseURL + 'reflect/user/';
    let data = '{"id": '+user.id+'}';
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    return this.http.delete(url, httpOptions);
  } */

  deleteClient(client: any): any {
    let url = API_REFLECT_URL + "delete_client.php";
    console.log(client);
    client.active = 0;
    let data = client
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateClient(client: any) {
    let url = API_REFLECT_URL + "update_client.php";
    let data = JSON.stringify(client);
    let response: any;
    console.log(client);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}

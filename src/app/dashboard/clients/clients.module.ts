import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ClientsPage } from "./clients-list/clients.page";
import { ClientsAddComponent } from "./clients-add/clients-add.component";
import { ImageCropperModule } from "ngx-image-cropper";
//import { DriversComponent } from "./users-drivers/drivers/drivers.component";

const routes: Routes = [
  {
    path: "",
    component: ClientsPage
  },
  {
    path: "add",
    component: ClientsAddComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ClientsPage,
    ClientsAddComponent,
  ],
  entryComponents: []
})
export class ClientsPageModule {}

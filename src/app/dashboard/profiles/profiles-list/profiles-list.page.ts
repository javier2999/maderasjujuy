import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfilesService } from '../profiles.service';
import { ModalController, AlertController } from '@ionic/angular';
import { ExcelServiceProvider } from '../../../services/excelService/excel-service.service';
import { ScreenService } from '../../../app-screen.service';
import { ProfilesAddComponent } from '../profiles-add/profiles-add.component';

@Component({
  selector: 'app-profiles-list',
  templateUrl: './profiles-list.page.html',
  styleUrls: ['./profiles-list.page.scss'],
})
export class ProfilesListPage implements OnInit {
  @ViewChild("inputSearch",{static:true}) inputSearch;
  profiles:any;
  profilesSinFiltro:any;
  isLoading=false;
  searchProfile:Object={profile:"",offset:0, limit:200};
  global_vars:any;
  infiniteRow:number=100;
  hide_sm;
  profile:string="";
  dataStatus:any;
  editModal:any;
  screen:any;
  constructor(
   private service:ProfilesService,
    public modalController:ModalController,
    public alertController:AlertController,
    public exelservice:ExcelServiceProvider,
    private screenService:ScreenService
  ) { 
    screenService.screen.subscribe(_screen =>{
      this.screen=_screen;
    });
    this.getProfile();
    this.hide_sm=false;
  }

  ngOnInit() {
  }
  getProfile(){

    this.searchProfile["profile"] = this.profile;
    this.searchProfile["offset"] = 0;
    this.searchProfile["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getprofileGroup(this.searchProfile)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        
        } else {
          this.profiles = this.dataStatus.profiles;
          console.log(this.profiles);
          this.profilesSinFiltro = this.dataStatus;
          
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;

   }
   async edit(profileIndex: any) {
    let group = this.profiles[profileIndex];
    this.editModal = await this.modalController.create({
      component: ProfilesAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        groupPar: group,
        idx: profileIndex,
        onSave: this.saveProfileEdited,
        onCancel: this.cancelProfileEdition
      }
    });
    return await this.editModal.present();
  }
  async addProfile() {
    let profilePar = {
      id: null,
      description: "",
      screen:"",
      active: 1,
      "profile-group": null


    };
    this.editModal = await this.modalController.create({
      component: ProfilesAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        profilePar: profilePar,
        idx: null,
        onSave: this.addNewProfile,
        onCancel: this.cancelProfileEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.exelservice.exportAsExcelFile(this.profiles, "profiles");
  }
  saveProfileEdited = (profileInfo: any, profileIndex: any) => {
    this.profiles[profileIndex] = profileInfo;
    this.editModal.dismiss();
  };
  addNewProfile = (profileInfo: any, profileIndex: any) => {
    this.profiles.push(profileInfo);
    this.editModal.dismiss();
  };
  cancelProfileEdition = () => {
    this.editModal.dismiss();
  };
  async delete(profileIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Perfil",
      message: "¿Confirma que quiere borrar el Perfil?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let group = this.profiles[profileIndex];
            let par = { id: group.id, active: 0 };

            this.service
              .deleteProfile(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getProfile();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }
  initializeItems() {
    this.profiles = this.profilesSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

   
    const val = ev.target.value;

    
    if (val && val.trim() != "") {
      this.profiles = this.profiles.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.profiles.length == 0) {
        this.getProfile();
      }
    }
  }

}

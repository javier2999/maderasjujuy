import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProfilesListPage } from './profiles-list/profiles-list.page';
import { ProfilesAddComponent } from './profiles-add/profiles-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


const routes:Routes=[
  {
    path:"",
    component:ProfilesListPage
  },
  {
    path:"add",
    component:ProfilesAddComponent
  }
];

@NgModule({
  declarations: [
    ProfilesListPage,
    ProfilesAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ]
})
export class ProfilespageModule { }

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, NavParams, ToastController } from '@ionic/angular';
import { ProfilesService } from '../profiles.service';

@Component({
  selector: 'app-profiles-add',
  templateUrl: './profiles-add.component.html',
  styleUrls: ['./profiles-add.component.scss'],
})
export class ProfilesAddComponent {
  @Input() profilePar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;

  loader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  profile:any={
  id:null,
  description: "",
  screen: "",
  active:0,

  
  };
  profileForm:FormGroup;
  validation_messages:any;
  constructor(
    private fb:FormBuilder,
    private service:ProfilesService,
    private navParams:NavParams,
    public loadingController:LoadingController,
    public toastController:ToastController
  ) {
    this.profile=this.navParams.get("profilePar");
    this.onSave=this.navParams.get("onSaver");
    this.onCancel=this.navParams.get("onCancel");
    this.idx=this.navParams.get("idx");
    this.inputGroup=
    typeof this.profile["profile-group"] !== "undefined"&&
    this.profile["profile-group"] !==null
    ? this.profile["profile-group"][0].group[0].id
    :1;
    this.createForm();
    this.getGroups();
   }
  createForm() {
    this.validation_messages = {
      description: [
        {
          type: "minlength",
          message: "El grupo debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El grupo es obligatorio." }
      ],
     
      
    };
    this.profileForm = this.fb.group({
      id: this.profile.id,
      description: new FormControl(
        this.profile.description,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),

      
      active: this.profile.active,
     
    });
  }
  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .get_Groups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.profile.is_admin = 1;
    }
    this.profile.sys_group_id = groupId;
    this.profile.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  save() {
     
    let profile = this.profileForm.value;

    
    
    if (typeof profile.id !== "undefined" && profile.id !== null) {
      this.service
        .updateProfile(profile)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Perfil Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addProfile(profile)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el Perfil", "danger");
          } else {
            console.log(data);
            profile.id = this.dataStatus.insertId;
            this.presentToast("Perfil Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(profile, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  } 
  cancel() {
   
    this.onCancel();
  }

}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


const API_AUTH_URL=`${environment.API_PATH}auth`;
const API_REGLECT_URL=`${environment.API_PATH}`;

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
  baseURL:string;
  headers:any;
  constructor(public http:HttpClient, private storage:Storage) { }
  set_headers(){
    return new Promise(resolve=>{
    this.storage.get("jtw").then(val=>{
      this.headers=new HttpHeaders({
        "content-type":"aplication/json",
        Accept:"aplication/json",
        Authorization:"Bearer"+val
      });
      resolve();
    });
    console.log(this.storage);
    })
  }
  close:any;
  closeSession(){
    return new Promise(resolve=>{
      this.set_headers().then(()=>{
        this.http
        .post(API_AUTH_URL+`logout`,{},{headers:this.headers})
        .subscribe(data=>{
          this.storage.remove("maderera-dash-jwt");
          this.close=data;
          resolve(this.close)
        });
      });
    });
  }
  profile:any;
 resutProfile:any;
 profileImg:any;
getProfile(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
    this.http
    .get(API_AUTH_URL+"profile",{headers:this.headers})
    .subscribe(data=>{
      this.resutProfile=data;
      if(this.resutProfile.length>0){
      this.profile=this.resutProfile[0];
      this.profile.pic="";
      resolve(this.profile);}
      else{
        this.closeSession().then(()=>{
          resolve(this.resutProfile);
          });
        }
      });
    });
  });
 };
 getProfileImg(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .get(
        API_REGLECT_URL + 
        `picture?filters=group:${this.profile.id}&sort=id&order=Des&limit=1`,
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.profileImg=data;
        if(
          typeof this.profileImg.status !=="undefined"&&
          this.profileImg.status==false
        ){
          this.closeSession().then(()=>{
            resolve(this.profileImg);
          });
        }else{
          resolve(this.profileImg);
        }
      });
    });
  });
}
responseSetImg:any;
setProfileImg(_base64Img){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .post(
        API_REGLECT_URL+`/picture`,
        {pic: _base64Img, sys_group_id:this.profile.id},
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.responseSetImg=data;
        if(
          typeof this.responseSetImg.status !=="undefined" &&
          this.responseSetImg.status==false
        ) {
          this.closeSession().then(()=>{
            resolve(this.responseSetImg);
          });
        } else{
          resolve(this.responseSetImg);
        }
      });
    });
  });
}
perfiles: any;
getPerfil(param) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(API_REGLECT_URL + `profile/profile-group/group`, {
          headers: this.headers
        })
        .subscribe(data => {
          this.perfiles = data;
          if (
            typeof this.perfiles.status !== "undefined" &&
            this.perfiles.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.perfiles);
            });
          } else {
            resolve(this.perfiles);
          }
        });
    });
  });
}
_groups: any;
  get_Groups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REGLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this._groups = data;
            if (
              typeof this._groups.status !== "undefined" &&
              this._groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this._groups);
              });
            } else {
              resolve(this._groups);
            }
          });
      });
    });
  }
  getProfiles() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `profile`, { headers: this.headers })
          .subscribe(data => {
            this.perfiles = data;
            if (
              typeof this.perfiles.status !== "undefined" &&
              this.perfiles.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.perfiles);
              });
            } else {
              resolve(this.perfiles);
            }
          });
      });
    });
  }
  getprofileGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(`${API_AUTH_URL}getallprofile.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.perfiles = data;
            if (
              typeof this.perfiles.status !== "undefined" &&
              this.perfiles.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.perfiles);
              });
            } else {
              resolve(this.perfiles);
            }
          });
      });
    });
  }
  getPRofiles(profileId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + `profile?filters=a.id:${profileId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.profile = data;
            if (
              typeof this.profile.status !== "undefined" &&
              this.profile.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.profile);
              });
            } else {
              resolve(this.profile);
            }
          });
      });
    });
  }
  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  addProfile(profile: any): any {

    let url = API_REGLECT_URL + "create_profile.php";

    let dataProfile = JSON.stringify(profile);
    console.log(profile);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataProfile, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
      
            resolve(response);
          }
        });
      });
    });
  }
  deleteProfile(profile: any): any {
    let url = API_REGLECT_URL + "delete_profile.php";
    console.log(profile);
    profile.active = 0;
    let data = profile
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  updateProfile(profile: any) {
    let url = API_REGLECT_URL + "update_profile.php";
    let data = JSON.stringify(profile);
    let response: any;
    console.log(profile);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }


}

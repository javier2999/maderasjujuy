import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PaymentmethodListPage } from './paymentmethod-list/paymentmethod-list.page';
import { PaymentmethodAddComponent } from './paymentmethod-add/paymentmethod-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImageCropperComponent } from 'ngx-image-cropper';

const routes:Routes=[
  {
    path:"",
    component:PaymentmethodListPage
  },
  {
    path:"add",
    component: PaymentmethodAddComponent
   }
]


@NgModule({
  declarations: [
    PaymentmethodListPage,
    PaymentmethodAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
  //  ImageCropperComponent,
    RouterModule.forChild(routes)
  ]
})
export class PaymentmethodpageModule { }

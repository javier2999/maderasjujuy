import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, NavParams, ToastController } from '@ionic/angular';
import { PaymentmethodService } from '../paymentmethod.service';

@Component({
  selector: 'app-paymentmethod-add',
  templateUrl: './paymentmethod-add.component.html',
  styleUrls: ['./paymentmethod-add.component.scss'],
})
export class PaymentmethodAddComponent {
  @Input() paymentmethodPar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;

  loader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  paymentmethod:any={
  id:null,
  description: "",
  TipopId:"",
  active:0,

  };
  paymentmethodForm:FormGroup;
  validation_messages:any;
  constructor(
    private fb:FormBuilder,
    private service:PaymentmethodService,
    private navParams:NavParams,
    public loadingController:LoadingController,
    public toastController:ToastController
  ) { 
    this.paymentmethod=this.navParams.get("paymentmethodPar");
    this.onSave=this.navParams.get("onSaver");
    this.onCancel=this.navParams.get("onCancel");
    this.idx=this.navParams.get("idx");
    this.inputGroup=
    typeof this.paymentmethod["paymentmethod-group"] !== "undefined"&&
    this.paymentmethod["paymentmethod-group"] !==null
    ? this.paymentmethod["paymentmethod-group"][0].group[0].id
    :1;
    this.createForm();
    this.getGroups();
  }
  createForm() {
    this.validation_messages = {
      description: [
        {
          type: "minlength",
          message: "El metodo de pago debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El metodo de pago es obligatorio." }
      ],
     
      
    };
    this.paymentmethodForm = this.fb.group({
      id: this.paymentmethod.id,
      description: new FormControl(
        this.paymentmethod.description,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
    
      
      active: this.paymentmethod.active,
     
    });
  }
  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .get_Groups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.paymentmethod.is_admin = 1;
    }
    this.paymentmethod.sys_group_id = groupId;
    this.paymentmethod.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  save() {
     
    let paymentmethod = this.paymentmethodForm.value;

    
    
    if (typeof paymentmethod.id !== "undefined" && paymentmethod.id !== null) {
      this.service
        .update_Paymentmethod(paymentmethod)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("metodo de pago  Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .add_Paymentmethod(paymentmethod)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el metodo de pago", "danger");
          } else {
            console.log(data);
            paymentmethod.id = this.dataStatus.insertId;
            this.presentToast("metodo de pago Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(paymentmethod, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  } 
  cancel() {
   
    this.onCancel();
  }


}

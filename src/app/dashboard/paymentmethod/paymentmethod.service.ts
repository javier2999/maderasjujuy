import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

const API_AUTH_URL=`${environment.API_PATH}auth`;
const API_REGLECT_URL=`${environment.API_PATH}`;

@Injectable({
  providedIn: 'root'
})
export class PaymentmethodService {

  baseURL:string;
  headers:any;
    constructor(public http:HttpClient, private storage:Storage) { }
    set_headers(){
      return new Promise(resolve=>{
      this.storage.get("jtw").then(val=>{
        this.headers=new HttpHeaders({
          "content-type":"aplication/json",
          Accept:"aplication/json",
          Authorization:"Bearer"+val
        });
        resolve();
      });
      console.log(this.storage);
      })
    }
    close:any;
 closeSession(){
   return new Promise(resolve=>{
     this.set_headers().then(()=>{
       this.http
       .post(API_AUTH_URL+`logout`,{},{headers:this.headers})
       .subscribe(data=>{
         this.storage.remove("maderera-dash-jwt");
         this.close=data;
         resolve(this.close)
       });
     });
   });
 }
 Paymentmethod:any;
 resutPaymentmethodService:any;
 profileImg:any;
get_Paymentmethod(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
    this.http
    .get(API_AUTH_URL+"profile",{headers:this.headers})
    .subscribe(data=>{
      this.resutPaymentmethodService=data;
      if(this.resutPaymentmethodService.length>0){
      this.Paymentmethod=this.resutPaymentmethodService[0];
      this.Paymentmethod.pic="";
      resolve(this.Paymentmethod);}
      else{
        this.closeSession().then(()=>{
          resolve(this.resutPaymentmethodService);
          });
        }
      });
    });
  });
 };
 metodopago: any;
getMetodopago(param) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(API_REGLECT_URL + `Paymentmethod/Paymentmethod-group/group`, {
          headers: this.headers
        })
        .subscribe(data => {
          this.metodopago = data;
          if (
            typeof this.metodopago.status !== "undefined" &&
            this.metodopago.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.metodopago);
            });
          } else {
            resolve(this.metodopago);
          }
        });
    });
  });
}
_groups: any;
  get_Groups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REGLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this._groups = data;
            if (
              typeof this._groups.status !== "undefined" &&
              this._groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this._groups);
              });
            } else {
              resolve(this._groups);
            }
          });
      });
    });
  }
  getPaymentmethod() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `Paymentmethod`, { headers: this.headers })
          .subscribe(data => {
            this.metodopago = data;
            if (
              typeof this.metodopago.status !== "undefined" &&
              this.metodopago.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.metodopago);
              });
            } else {
              resolve(this.metodopago);
            }
          });
      });
    });
  }
  getPaymentmethodGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(`${API_AUTH_URL}getallpaymentmethod.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.metodopago = data;
            if (
              typeof this.metodopago.status !== "undefined" &&
              this.metodopago.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.metodopago);
              });
            } else {
              resolve(this.metodopago);
            }
          });
      });
    });
  }
  getPAymentmethod(paymentmethodId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + `paymentmethod?filters=a.id:${paymentmethodId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.Paymentmethod = data;
            if (
              typeof this.Paymentmethod.status !== "undefined" &&
              this.Paymentmethod.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.Paymentmethod);
              });
            } else {
              resolve(this.Paymentmethod);
            }
          });
      });
    });
  }
  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  add_Paymentmethod(paymentmethod: any): any {

    let url = API_REGLECT_URL + "create_paymentmethod.php";

    let dataPaymentmethod = JSON.stringify(paymentmethod);
    console.log(paymentmethod);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataPaymentmethod, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
      
            resolve(response);
          }
        });
      });
    });
  }
  delete_Paymentmethod(paymentmethod: any): any {
    let url = API_REGLECT_URL + "delete_paymentmethod.php";
    console.log(paymentmethod);
    paymentmethod.active = 0;
    let data = paymentmethod
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  update_Paymentmethod(paymentmethod: any) {
    let url = API_REGLECT_URL + "update_paymentmethod.php";
    let data = JSON.stringify(paymentmethod);
    let response: any;
    console.log(paymentmethod);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}

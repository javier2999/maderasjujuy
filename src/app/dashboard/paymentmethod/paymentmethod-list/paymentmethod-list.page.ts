import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { PaymentmethodService } from '../paymentmethod.service';
import { ExcelServiceProvider } from '../../../services/excelService/excel-service.service';
import { ScreenService } from '../../../app-screen.service';
import { PaymentmethodAddComponent } from '../paymentmethod-add/paymentmethod-add.component';

@Component({
  selector: 'app-paymentmethod-list',
  templateUrl: './paymentmethod-list.page.html',
  styleUrls: ['./paymentmethod-list.page.scss'],
})
export class PaymentmethodListPage implements OnInit {

  @ViewChild("inputSearch",{static:true}) inputSearch;
  paymentmethods:any;
  paymentmethodSinFiltro:any;
  isLoading=false;
  searchpaymentmethod:Object={paymentmethod:"",offset:0, limit:200};
  global_vars:any;
  infiniteRow:number=100;
  hide_sm;
  paymentmethod:string="";
  dataStatus:any;
  editModal:any;
  screen:any;
  constructor(
    private service:PaymentmethodService,
    public modalController:ModalController,
    public alertController:AlertController,
    public exelservice:ExcelServiceProvider,
    private screenService:ScreenService
  ) { 
    screenService.screen.subscribe(_screen =>{
      this.screen=_screen;
    });
    this.getPaymentmethod();
    this.hide_sm=false;
  }

  ngOnInit() {
  }
  getPaymentmethod(){

    this.searchpaymentmethod["paymentmethod"] = this.paymentmethod;
    this.searchpaymentmethod["offset"] = 0;
    this.searchpaymentmethod["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getPaymentmethodGroup(this.searchpaymentmethod)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        
        } else {
          this.paymentmethods = this.dataStatus.profiles;
          console.log(this.paymentmethods);
          this.paymentmethodSinFiltro = this.dataStatus;
          
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;

   }
   async edit(profileIndex: any) {
    let group = this.paymentmethods[profileIndex];
    this.editModal = await this.modalController.create({
      component: PaymentmethodAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        groupPar: group,
        idx: profileIndex,
        onSave: this.savePaymentmethodEdited,
        onCancel: this.cancelPaymentmethodEdition
      }
    });
    return await this.editModal.present();
  }
  async addPaymentmethods() {
    let paymentmethodPar = {
      id: null,
      description: "",
      TipopId:"",
      active: 1,
      "paymentmethod-group": null


    };
    this.editModal = await this.modalController.create({
      component: PaymentmethodAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        paymentmethodPar: paymentmethodPar,
        idx: null,
        onSave: this.addNewPaymentmethod,
        onCancel: this.cancelPaymentmethodEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.exelservice.exportAsExcelFile(this.paymentmethods, "paymentmethods");
  }
  savePaymentmethodEdited = (paymentmethodInfo: any, paymentmethodIndex: any) => {
    this.paymentmethods[paymentmethodIndex] = paymentmethodInfo;
    this.editModal.dismiss();
  };
  addNewPaymentmethod = (paymentmethodInfo: any, paymentmethodIndex: any) => {
    this.paymentmethods.push(paymentmethodInfo);
    this.editModal.dismiss();
  };
  cancelPaymentmethodEdition = () => {
    this.editModal.dismiss();
  };
  async delete(paymentmethodIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Metodo de pago",
      message: "¿Confirma que quiere borrar el Metodo de pago?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let group = this.paymentmethods[paymentmethodIndex];
            let par = { id: group.id, active: 0 };

            this.service
              .delete_Paymentmethod(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getPaymentmethod();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }
  initializeItems() {
    this.paymentmethods = this.paymentmethodSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

   
    const val = ev.target.value;

    
    if (val && val.trim() != "") {
      this.paymentmethods = this.paymentmethods.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.paymentmethods.length == 0) {
        this.getPaymentmethod();
      }
    }
  }

}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SellsListPage } from "./sells-list/sells-list.page";
import { SellsAddComponent } from "./sells-add/sells-add.component";
import { ImageCropperModule } from "ngx-image-cropper";
//import { DriversComponent } from "./sells-drivers/drivers/drivers.component";

const routes: Routes = [
  {
    path: "",
    component: SellsAddComponent
  },
  {
    path: "add",
    component: SellsAddComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    SellsListPage,
    SellsAddComponent,
  ],
})
export class SellsPageModule {}

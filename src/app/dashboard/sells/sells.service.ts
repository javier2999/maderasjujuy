import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: "root"
})
export class SellsService {
  baseURL: String;
  headers: any;
  productos: any;
  product: any;
  products: any;
  resultProduct: any;
  constructor(public http: HttpClient, private storage: Storage) {}

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  getProducts() {
    return new Promise(resolve => {
      var a = {codbar: "true"};
      let dataProduct = JSON.stringify(a);
      console.log(dataProduct);
      this.set_headers().then(() => {
        this.http
          .post(API_REFLECT_URL + `get_products.php`, dataProduct,{ headers: this.headers })
          .subscribe(data => {
            this.products = data;
            if (
              typeof this.products.status !== "undefined" &&
              this.products.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.products);
              });
            } else {
              resolve(this.products);
            }
          });
      });
    });
  }
}

import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController,ToastController } from "@ionic/angular";

import { SellsService } from "../sells.service";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-sells-add',
  templateUrl: './sells-add.component.html',
  styleUrls: ['./sells-add.component.scss'],
})
export class SellsAddComponent implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  products: any;
  product: any;
  productsCarrito: any;
  productsSinFiltro: any;
  isLoading = false;
  inputFdp: number = 0;
  inputCliente: number = 0;
  searchProduct: Object = { sell: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  sell: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;
  ventafinal: any;
  fdpForm: FormGroup;
  clienteForm: FormGroup;

  constructor(
    private service: SellsService,
    private fb: FormBuilder,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService,
    public toastController: ToastController
  ) {
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });
    this.createForm();
    this.getProducts();

    this.hide_sm = false;
    this.productsCarrito = [];

    this.products = [
      {
        id: 1,
        codbar: "12345678",
        descrip: "Producto de Prueba 1",
        cost_price: 123.58,
        sell_price: 250.58,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 2,
        codbar: "12345678",
        descrip: "Producto de Prueba 2",
        cost_price: 43.1,
        sell_price: 546.6,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 3,
        codbar: "12345678",
        descrip: "Producto de Prueba 3",
        cost_price: 33.43,
        sell_price: 545.7,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 4,
        codbar: "12345678",
        descrip: "Producto de Prueba 4",
        cost_price: 74.4,
        sell_price: 845.12,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 5,
        codbar: "12345678",
        descrip: "Producto de Prueba 5",
        cost_price: 22.58,
        sell_price: 35.18,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      }

    ];

    this.productsSinFiltro = [
      {
        id: 1,
        codbar: "12345678",
        descrip: "Producto de Prueba 1",
        cost_price: 123.58,
        sell_price: 250.58,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 2,
        codbar: "12345678",
        descrip: "Producto de Prueba 2",
        cost_price: 43.1,
        sell_price: 546.6,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 3,
        codbar: "12345678",
        descrip: "Producto de Prueba 3",
        cost_price: 33.43,
        sell_price: 545.7,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 4,
        codbar: "12345678",
        descrip: "Producto de Prueba 4",
        cost_price: 74.4,
        sell_price: 845.12,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      },
      {
        id: 5,
        codbar: "12345678",
        descrip: "Producto de Prueba 5",
        cost_price: 22.58,
        sell_price: 35.18,
        stock: 20,
        unityid: 1,
        tipprodid: 1,
        active: 1
      }

    ];
    
  }

  //this.product =

  ngOnInit() {
    //this.getProducts();
  }

  createForm(){
    this.fdpForm = this.fb.group({
      sys_fdp_id: this.inputFdp
    })
    this.clienteForm = this.fb.group({
      sys_cliente_id: this.inputCliente
    })
  }

  getProducts() {
    this.searchProduct = this.sell;
  
    this.isLoading = true;
    this.service
      .getProducts()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        } else {
          this.products = this.dataStatus.products;
          this.productsSinFiltro = this.dataStatus;
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;
  }

 /* async edit(productIndex: any) {
    let sell = this.sells[productIndex];
    this.editModal = await this.modalController.create({
      component: ProductsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        productPar: sell,
        idx: productIndex,
        onSave: this.saveProductEdited,
        onCancel: this.cancelProductEdition
      }
    });
    return await this.editModal.present();
  }

  async fraccionar(productIndex: any) {
    let sell = this.sells[productIndex];
    this.editModal = await this.modalController.create({
      component: ProductsFracComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        productPar: sell,
        idx: productIndex,
        onSave: this.saveProductFraccionado,
        onCancel: this.cancelProductEdition
      }
    });
    return await this.editModal.present();
  }*/

/*  async addProduct() {
    let productPar = {
      id: null,
      dni: "",
      last_name: "",
      first_name: "",
      phone: "",
      email: "",
      password: "",
      active: 1,
      is_admin: 1,
      sys_tipo_id: 1,
      "sell-tipo": null
    };
    this.editModal = await this.modalController.create({
      component: ProductsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        productPar: productPar,
        idx: null,
        onSave: this.addNewProduct,
        onCancel: this.cancelProductEdition
      }
    });
    return await this.editModal.present();
  }
*/
  downloadExcel() {
    this.excelService.exportAsExcelFile(this.ventafinal, "sells");
  }

 /* saveProductEdited = (productInfo: any, productIndex: any) => {
    this.sells[productIndex] = productInfo;
    this.editModal.dismiss();
  };

  saveProductFraccionado = (productInfo: any, productIndex: any) => {
    this.sells[productIndex] = productInfo;
    this.editModal.dismiss();
  };*/

  /*addNewProduct = (productInfo: any, productIndex: any) => {
    this.sells.push(productInfo);
    this.editModal.dismiss();
  };*/

  cancelProductEdition = () => {
    this.editModal.dismiss();
  };

  /*async delete(productIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Producto",
      message: "¿Confirma que quiere borrar el producto?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let sell = this.sells[productIndex];
            let par = { id: sell.id, active: 0 };

            this.service
              .deleteProduct(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getProducts();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }*/

  initializeItems() {
    this.products = this.productsSinFiltro;
  }

  getItems(ev: any) {

    /* if (ev.cancelable) {
      this.getProducts();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.products = this.productsSinFiltro.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.products.length == 0) {
        this.products = this.productsSinFiltro;
      }
    }
  }

  async addCarrito(productIndex: any) {
    let product = this.products[productIndex];

    if(this.productsCarrito.length == 0){

      this.productsCarrito = []
    }
    this.productsCarrito.push(product);

    var total = this.productsCarrito.sells_price;
    this.presentToast("Producto agregado correctamente", "success");
    this.product = "";

    return true;
  }

  async addCantidad(productIndex: any) {
    let product = this.products[productIndex];
    
    if(this.productsCarrito.length == 0){

      this.productsCarrito = []
    }
    this.productsCarrito.push(product);

    var total = this.productsCarrito.sells_price;
    this.presentToast("Cantidad sumada correctamente", "success");
    this.product = "";

    return true;
  }

  async restCantidad(productIndex: any) {
    let product = this.products[productIndex];
    
    if(this.productsCarrito.length == 0){

      this.productsCarrito = []
    }
    this.productsCarrito.push(product);

    var total = this.productsCarrito.sells_price;
    this.presentToast("Producto restado correctamente", "success");
    this.product = "";

    return true;
  }

  async deleteProd(productIndex: any) {
    let product = this.products[productIndex];
    
    if(this.productsCarrito.length == 0){

      this.productsCarrito = []
    }
    this.productsCarrito.push(product);

    var total = this.productsCarrito.sells_price;
    this.presentToast("Producto quitado correctamente", "success");
    this.product = "";

    return true;
  }

  async presentToast(_message, _color) {
    let toast = await this.toastController.create({
      message: _message,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  
}

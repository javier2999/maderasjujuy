import { TestBed } from '@angular/core/testing';

import { TypesofcustomerService } from './typesofcustomer.service';

describe('TypesofcustomerService', () => {
  let service: TypesofcustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypesofcustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

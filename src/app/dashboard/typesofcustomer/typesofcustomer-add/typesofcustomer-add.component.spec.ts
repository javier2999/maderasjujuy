import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TypesofcustomerAddComponent } from './typesofcustomer-add.component';

describe('TypesofcustomerAddComponent', () => {
  let component: TypesofcustomerAddComponent;
  let fixture: ComponentFixture<TypesofcustomerAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypesofcustomerAddComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TypesofcustomerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

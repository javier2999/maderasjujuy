import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, NavParams, ToastController } from '@ionic/angular';
import { TypesofcustomerService } from '../typesofcustomer.service';

@Component({
  selector: 'app-typesofcustomer-add',
  templateUrl: './typesofcustomer-add.component.html',
  styleUrls: ['./typesofcustomer-add.component.scss'],
})
export class TypesofcustomerAddComponent {
  @Input() TypesofcustomerPar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;

  loader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  typesofcustomer:any={
  id: null,
  description: "",
  active:0,


  
  };
  typesofcustomerForm:FormGroup;
  validation_messages:any;
  constructor(
    private fb:FormBuilder,
    private service:TypesofcustomerService,
    private navParams:NavParams,
    public loadingController:LoadingController,
    public toastController:ToastController
  ) {
    this.typesofcustomer=this.navParams.get("TypesofcustomerPar");
    this.onSave=this.navParams.get("onSaver");
    this.onCancel=this.navParams.get("onCancel");
    this.idx=this.navParams.get("idx");
    this.inputGroup=
    typeof this.typesofcustomer["typesofcustomer-group"] !== "undefined"&&
     this.typesofcustomer["typesofcustomer-group"] !==null
    ? this.typesofcustomer["typesofcustomer-group"][0].group[0].id
    :1;
    this.createForm();
    this.getGroups();
  }
  createForm() {
    this.validation_messages = {
      description: [
        {
          type: "minlength",
          message: "El Tipo de Cliente debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El Tipo de Cliente es obligatorio." }
      ],
     
      
    };
    this.typesofcustomerForm = this.fb.group({
      id: this.typesofcustomer.id,
      description: new FormControl(
        this.typesofcustomer.description,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
    
      
      active: this.typesofcustomer.active,
     
    });
  }
  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .get_Groups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.typesofcustomer.is_admin = 1;
    }
    this.typesofcustomer.sys_group_id = groupId;
    this.typesofcustomer.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  save() {
     
    let Typeofcustomer = this.typesofcustomerForm.value;

    
    
    if (typeof Typeofcustomer.id !== "undefined" && Typeofcustomer.id !== null) {
      this.service
        .update_Typesofcustomer(Typeofcustomer)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("tipo cliente  Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .add_typesofcustomer(Typeofcustomer)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el tipo cliente", "danger");
          } else {
            console.log(data);
            Typeofcustomer.id = this.dataStatus.insertId;
            this.presentToast("tipo cliente Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(Typeofcustomer, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  } 
  cancel() {
   
    this.onCancel();
  }

}

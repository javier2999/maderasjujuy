import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TypesofcustomerListPage } from './typesofcustomer-list/typesofcustomer-list.page';
import { TypesofcustomerAddComponent } from './typesofcustomer-add/typesofcustomer-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


const routes:Routes=[
{
  path:"",
  component:TypesofcustomerListPage
},{
  path:"add",
  component:TypesofcustomerAddComponent
}

]

@NgModule({
  declarations: [
    TypesofcustomerListPage,
    TypesofcustomerAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ]
})
export class TypesofcustomerpageModule { }

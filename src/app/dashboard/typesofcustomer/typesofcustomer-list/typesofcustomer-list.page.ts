import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { TypesofcustomerService } from '../typesofcustomer.service';
import { ExcelServiceProvider } from '../../../services/excelService/excel-service.service';
import { ScreenService } from '../../../app-screen.service';
import { TypesofcustomerAddComponent } from '../typesofcustomer-add/typesofcustomer-add.component';

@Component({
  selector: 'app-typesofcustomer-list',
  templateUrl: './typesofcustomer-list.page.html',
  styleUrls: ['./typesofcustomer-list.page.scss'],
})
export class TypesofcustomerListPage implements OnInit {
  @ViewChild("inputSearch",{static:true}) inputSearch;
  typesofcustomers:any;
  typesofcustomerSinFiltro:any;
  isLoading=false;
  searchtypesofcustomer:Object={typesofcustomer:"",offset:0, limit:200};
  global_vars:any;
  infiniteRow:number=100;
  hide_sm;
  typesofcustomer:string="";
  dataStatus:any;
  editModal:any;
  screen:any;
  constructor(
    private service:TypesofcustomerService,
    public modalController:ModalController,
    public alertController:AlertController,
    public exelservice:ExcelServiceProvider,
    private screenService:ScreenService
  ) {
    screenService.screen.subscribe(_screen =>{
      this.screen=_screen;
    });
    this.getTypesofcustomer();
    this.hide_sm=false;
   }

  ngOnInit() {
  }
  getTypesofcustomer(){

    this.searchtypesofcustomer["typesofcustomer"] = this.typesofcustomer;
    this.searchtypesofcustomer["offset"] = 0;
    this.searchtypesofcustomer["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getTypesofcustomerGroup(this.searchtypesofcustomer)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        
        } else {
          this.typesofcustomers = this.dataStatus.profiles;
          console.log(this.typesofcustomers);
          this.typesofcustomerSinFiltro = this.dataStatus;
          
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;

   }
   async edit(typesofcustomerIndex: any) {
    let group = this.typesofcustomers[typesofcustomerIndex];
    this.editModal = await this.modalController.create({
      component: TypesofcustomerAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        groupPar: group,
        idx: typesofcustomerIndex,
        onSave: this.saveTypesofcustomerEdited,
        onCancel: this.cancelTypesofcustomerEdition
      }
    });
    return await this.editModal.present();
  }
  async addTypesofcustomers() {
    let TypesofcustomerPar = {
      id: null,
      description: "",
      active: 1,
      "Typesofcustomer-group": null


    };
    this.editModal = await this.modalController.create({
      component: TypesofcustomerAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        TypesofcustomerPar: TypesofcustomerPar,
        idx: null,
        onSave: this.addNewtypesofcustomer,
        onCancel: this.cancelTypesofcustomerEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.exelservice.exportAsExcelFile(this.typesofcustomers, "typesofcustomers");
  }
  saveTypesofcustomerEdited = (typesofcustomersInfo: any, typesofcustomerIndex: any) => {
    this.typesofcustomers[typesofcustomerIndex] = typesofcustomersInfo;
    this.editModal.dismiss();
  };
  addNewtypesofcustomer= (typesofcustomersInfo: any, typesofcustomerIndex: any) => {
    this.typesofcustomers.push(typesofcustomersInfo);
    this.editModal.dismiss();
  };
  cancelTypesofcustomerEdition = () => {
    this.editModal.dismiss();
  };
  async delete(typesofcustomerIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar tipo de cliente",
      message: "¿Confirma que quiere borrar el tipo de cliente?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let group = this.typesofcustomers[typesofcustomerIndex];
            let par = { id: group.id, active: 0 };

            this.service
              .delete_Typesofcustomer(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getTypesofcustomer();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }
  initializeItems() {
    this.typesofcustomers = this.typesofcustomerSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

   
    const val = ev.target.value;

    
    if (val && val.trim() != "") {
      this.typesofcustomers = this.typesofcustomers.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.typesofcustomers.length == 0) {
        this.getTypesofcustomer();
      }
    }
  }


}

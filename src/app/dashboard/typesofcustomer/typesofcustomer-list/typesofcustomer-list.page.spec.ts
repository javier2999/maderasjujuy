import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TypesofcustomerListPage } from './typesofcustomer-list.page';

describe('TypesofcustomerListPage', () => {
  let component: TypesofcustomerListPage;
  let fixture: ComponentFixture<TypesofcustomerListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypesofcustomerListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TypesofcustomerListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

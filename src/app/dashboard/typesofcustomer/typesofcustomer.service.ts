import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


const API_AUTH_URL=`${environment.API_PATH}auth`;
const API_REGLECT_URL=`${environment.API_PATH}`;

@Injectable({
  providedIn: 'root'
})
export class TypesofcustomerService {
  baseURL:string;
  headers:any;
  constructor(public http:HttpClient, private storage:Storage) { }
 
  set_headers(){
    return new Promise(resolve=>{
    this.storage.get("jtw").then(val=>{
      this.headers=new HttpHeaders({
        "content-type":"aplication/json",
        Accept:"aplication/json",
        Authorization:"Bearer"+val
      });
      resolve();
    });
    console.log(this.storage);
    })
  }
  close:any;
  closeSession(){
    return new Promise(resolve=>{
      this.set_headers().then(()=>{
        this.http
        .post(API_AUTH_URL+`logout`,{},{headers:this.headers})
        .subscribe(data=>{
          this.storage.remove("maderera-dash-jwt");
          this.close=data;
          resolve(this.close)
        });
      });
    });
  }
  Typesofcustomer:any;
 resutTypesofcustomerService:any;
 profileImg:any;
get_Paymentmethod(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
    this.http
    .get(API_AUTH_URL+"Typesofcustomer",{headers:this.headers})
    .subscribe(data=>{
      this.resutTypesofcustomerService=data;
      if(this.resutTypesofcustomerService.length>0){
      this.Typesofcustomer=this.resutTypesofcustomerService[0];
      this.Typesofcustomer.pic="";
      resolve(this.Typesofcustomer);}
      else{
        this.closeSession().then(()=>{
          resolve(this.resutTypesofcustomerService);
          });
        }
      });
    });
  });
 };
 TipoCliente: any;
getTipoCliente(param) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(API_REGLECT_URL + `Typesofcustomer/Typesofcustomer-group/group`, {
          headers: this.headers
        })
        .subscribe(data => {
          this.TipoCliente = data;
          if (
            typeof this.TipoCliente.status !== "undefined" &&
            this.TipoCliente.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.TipoCliente);
            });
          } else {
            resolve(this.TipoCliente);
          }
        });
    });
  });
}
_groups: any;
  get_Groups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REGLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this._groups = data;
            if (
              typeof this._groups.status !== "undefined" &&
              this._groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this._groups);
              });
            } else {
              resolve(this._groups);
            }
          });
      });
    });
  }
  getTypesofcustomer() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `Typesofcustomer`, { headers: this.headers })
          .subscribe(data => {
            this.TipoCliente = data;
            if (
              typeof this.TipoCliente.status !== "undefined" &&
              this.TipoCliente.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.TipoCliente);
              });
            } else {
              resolve(this.TipoCliente);
            }
          });
      });
    });
  }
  getTypesofcustomerGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(`${API_AUTH_URL}getalltypesofcustomer.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.TipoCliente = data;
            if (
              typeof this.TipoCliente.status !== "undefined" &&
              this.TipoCliente.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.TipoCliente);
              });
            } else {
              resolve(this.TipoCliente);
            }
          });
      });
    });
  }
  getTYpesofcustomer(typesofcustomer: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + `typesofcustomer?filters=a.id:${typesofcustomer}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.Typesofcustomer = data;
            if (
              typeof this.Typesofcustomer.status !== "undefined" &&
              this.Typesofcustomer.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.Typesofcustomer);
              });
            } else {
              resolve(this.Typesofcustomer);
            }
          });
      });
    });
  }
  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  add_typesofcustomer(typesofcustomer: any): any {

    let url = API_REGLECT_URL + "create_typesofcustomer.php";

    let dataTypesofcustomer = JSON.stringify(typesofcustomer);
    console.log(typesofcustomer);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataTypesofcustomer, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
      
            resolve(response);
          }
        });
      });
    });
  }
  delete_Typesofcustomer(typesofcustomer: any): any {
    let url = API_REGLECT_URL + "delete_typesofcustomer.php";
    console.log(typesofcustomer);
    typesofcustomer.active = 0;
    let data = typesofcustomer
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  update_Typesofcustomer(Typesofcustomer: any) {
    let url = API_REGLECT_URL + "update_Typesofcustomer.php";
    let data = JSON.stringify(Typesofcustomer);
    let response: any;
    console.log(Typesofcustomer);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}

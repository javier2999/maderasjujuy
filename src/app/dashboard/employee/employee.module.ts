import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListPage } from './employee-list/employee-list.page';
import { EmployeeAddComponent } from './employee-add/employee-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImageCropperModule } from 'ngx-image-cropper';

const routes: Routes=[
{
  path:"",
  component:EmployeeListPage
},
{
 path:"add",
 component:EmployeeAddComponent
}

];

@NgModule({
  declarations: [
    EmployeeListPage,
    EmployeeAddComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ]
})
export class EmployeepageModule { }

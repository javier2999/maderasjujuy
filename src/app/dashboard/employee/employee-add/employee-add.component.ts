import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
//import { time, timeStamp } from 'console';
import { EmployeeService } from '../employee.service';
import { NavParams, LoadingController, ToastController, IonDatetime } from '@ionic/angular';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss'],
})
export class EmployeeAddComponent {
  @Input() employeePar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;

  loader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  employee:any={
  id:null,
  last_name: "",
  first_name: "",
  fullname:"last_name + first_name",
  dni:"",
  address:"",
  salary:"",
  phone: "",
  date:"",
  active:0,



  };

  employeeForm:FormGroup;
  validation_messages:any;

  constructor(
   private fb:FormBuilder,
   private service:EmployeeService,
   private navParams:NavParams,
   public loadingController:LoadingController,
   public toastController:ToastController
 ) {
    this.employee=this.navParams.get("employeePar");
    this.onSave=this.navParams.get("onSave");
    this.onCancel=this.navParams.get("onCancel");
    this.idx=this.navParams.get("idx");
    this.inputGroup=
    typeof this.employee["employee-group"] !== "undefined"&&
    this.employee["employee-group"] !==null
    ? this.employee["employee-group"][0].group[0].id
    :1;
    this.createForm();
    this.getGroups();
  }
  createForm() {
    this.validation_messages = {
      dni: [
        { type: "required", message: "El DNI es obligatorio." },
        {
          type: "pattern",
          message:
            "El DNI debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      first_name: [
        {
          type: "minlength",
          message: "El nombre debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El nombre es obligatorio." }
      ],
      last_name: [
        {
          type: "minlength",
          message: "El apellido debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El apellido es obligatorio." }
      ],
      address: [
        {
          type: "minlength",
          message: "ingrese domicilio"
        },
        { type: "required", message: "El domicilio es obligatorio." },
      ],


    };
    this.employeeForm = this.fb.group({
      id: this.employee.id,
      dni: new FormControl(
        this.employee.dni,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\d{7,9}$")
        ])
      ),
      last_name: new FormControl(
        this.employee.last_name,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      first_name: new FormControl(
        this.employee.first_name,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      phone: this.employee.phone,
      salary:this.employee.salary,
      address: new FormControl(
        this.employee.address,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),



      active: this.employee.active,

    });
  }   
      
  
  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .getGroups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.employee.is_admin = 1;
    }
    this.employee.sys_group_id = groupId;
    this.employee.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  save() {
     
    let employee = this.employeeForm.value;

    
    
    if (typeof employee.id !== "undefined" && employee.id !== null) {
      this.service
        .updateEployee(employee)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Empleado Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addEmployee(employee)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el usuario", "danger");
          } else {
            console.log(data);
            employee.id = this.dataStatus.insertId;
            this.presentToast("Usuario Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(employee, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  }
  cancel() {
   
    this.onCancel();
  }

}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

const API_AUTH_URL=`${environment.API_PATH}`;
const API_REGLECT_URL=`${environment.API_PATH}`;


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  baseURL:String;
  headers:any;

  constructor(public http:HttpClient, private storage:Storage) { }

  set_headers(){
   return new Promise(resolve=>{
     this.storage.get("jwt").then(val=>{
     this.headers=new HttpHeaders({
      "Content-Type":"application/json",
      Accept:"application/json",
      Authorization:"Bearer"+val
    });
    resolve();
     });
     console.log(this.storage);
   });
  }
 close:any;
 closeSession(){
 return new Promise(resolve=>{
 this.set_headers().then(()=>{
 this.http
 .post(API_AUTH_URL+`logout`,{},{headers:this.headers})
 .subscribe(data=>{
   this.storage.remove("maderera-dash-jwt");
   this.close=data;
   resolve(this.close)
     });
   });
 });
}

employee:any;
resutEmployee:any;
profileImg:any;
getEmployee(){
 return new Promise(resolve=>{
   this.set_headers().then(()=>{
   this.http
   .get(API_AUTH_URL+"employee",{headers:this.headers})
   .subscribe(data=>{
     this.resutEmployee=data;
     if(this.resutEmployee.length>0){
     this.employee=this.resutEmployee[0];
     this.employee.pic="";
     resolve(this.employee);}
     else{
       this.closeSession().then(()=>{
         resolve(this.resutEmployee);
         });
       }
     });
   });
 });
};
getProfileImg(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .get(
        API_REGLECT_URL + 
        `picture?filters=sys_employee_id:${this.employee.id}&sort=id&order=Des&limit=1`,
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.profileImg=data;
        if(
          typeof this.profileImg.status !=="undefined"&&
          this.profileImg.status==false
        ){
          this.closeSession().then(()=>{
            resolve(this.profileImg);
          });
        }else{
          resolve(this.profileImg);
        }
      });
    });
  });
}

responseSetImg:any;
setProfileImg(_base64Img){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .post(
        API_REGLECT_URL+`/picture`,
        {pic: _base64Img, sys_employee_id:this.employee.id},
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.responseSetImg=data;
        if(
          typeof this.responseSetImg.status !=="undefined" &&
          this.responseSetImg.status==false
        ) {
          this.closeSession().then(()=>{
            resolve(this.responseSetImg);
          });
        } else{
          resolve(this.responseSetImg);
        }
      });
    });
  });
}
// -------------------------- Local DB ------------------------- //
 empleados: any;
  getEmpleado(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `employee/employee-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.empleados = data;
            if (
              typeof this.empleados.status !== "undefined" &&
              this.empleados.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.empleados);
              });
            } else {
              resolve(this.empleados);
            }
          });
      });
    });
  }

  groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REGLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }

  getEmployees() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `employee`, { headers: this.headers })
          .subscribe(data => {
            this.empleados = data;
            if (
              typeof this.empleados.status !== "undefined" &&
              this.empleados.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.empleados);
              });
            } else {
              resolve(this.empleados);
            }
          });
      });
    });
  }
 //acá hace la llamada al grid de employee-list
 getemployeesGroup(param) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(`${API_AUTH_URL}get_all_employee.php`,
          {
            headers: this.headers
          }
        )
        .subscribe(data => {
          this.empleados = data;
          if (
            typeof this.empleados.status !== "undefined" &&
            this.empleados.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.empleados);
            });
          } else {
            resolve(this.empleados);
          }
        });
    });
  });
}
 getEMployee(employeeId: number) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(API_AUTH_URL + `employee?filters=a.id:${employeeId}`, {
          headers: this.headers
        })
        .subscribe(data => {
          this.employee = data;
          if (
            typeof this.employee.status !== "undefined" &&
            this.employee.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.employee);
            });
          } else {
            resolve(this.employee);
          }
        });
    });
  });
}
 permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  addEmployee(employee: any): any {

    let url = API_REGLECT_URL + "create_employee.php";

    let dataEmployee = JSON.stringify(employee);
    console.log(employee);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataEmployee, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
      
            resolve(response);
          }
        });
      });
    });
  }
  deleteEmployee(employee: any): any {
    let url = API_REGLECT_URL + "delete_employee.php";
    console.log(employee);
    employee.active = 0;
    let data = employee
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  updateEployee(employee: any) {
    let url = API_REGLECT_URL + "update_employee.php";
    let data = JSON.stringify(employee);
    let response: any;
    console.log(employee);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }

}

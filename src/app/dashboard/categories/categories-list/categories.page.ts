import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../../users/users.service';
import { ModalController, AlertController } from '@ionic/angular';
import { ExcelServiceProvider } from '../../../services/excelService/excel-service.service';
import { ScreenService } from '../../../app-screen.service';
import { CategoriesAddComponent } from '../categories-add/categories-add.component';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  categories:any;
  CategoriesSinFiltro:any;
  isLoading=false;
  searchCategory:object={ user: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  category: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: UsersService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService

  ) { 
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });
    this.getCategories();
    this.hide_sm = false;
  }
  
  ngOnInit() {
    //this.getUsers();
  }
  getCategories() {
    this.searchCategory["user"] = this.category;
    this.searchCategory["offset"] = 0;
    this.searchCategory["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getUsersGroup(this.searchCategory)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.categories = this.dataStatus.categories;
          console.log(this.categories);
          this.CategoriesSinFiltro = this.dataStatus;
          //console.log(this.users);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;
  }
  async edit(categoryIndex: any) {
    let category = this.categories[categoryIndex];
    this.editModal = await this.modalController.create({
      component:CategoriesAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        categoryPar: category,
        idx: categoryIndex,
        onSave: this.saveCategoryEdited,
        onCancel: this.cancelCategoryEdition
      }
    });
    return await this.editModal.present();
  }
  async addCategory() {
    let categoryPar = {
      id: null,
      descrip:"",
      active:1,
      
      "user-group": null,
      
     
    };
    this.editModal = await this.modalController.create({
      component: CategoriesAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        categoryPar: categoryPar,
        idx: null,
        onSave: this.addNewCategory,
        onCancel: this.cancelCategoryEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.excelService.exportAsExcelFile(this.categories, "categories");
  }

  saveCategoryEdited = (categoryInfo: any, categoryIndex: any) => {
    this.categories[categoryIndex] = categoryInfo;
    this.editModal.dismiss();
  };

  addNewCategory = (categoryInfo: any, categoryIndex: any) => {
    //this.categories.push(categoryInfo);
    this.categories.push(categoryInfo);
    this.editModal.dismiss();
  };

  cancelCategoryEdition = () => {
    this.editModal.dismiss();
  };
  async delete(categoryIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Categoria",
      message: "¿Confirma que quiere borrar la categoria?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let category = this.categories[categoryIndex];
            let par = { id: category.id, active: 0 };

            this.service
              .deleteUser(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getCategories();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }

  initializeItems() {
    this.categories = this.CategoriesSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getUsers();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.categories = this.categories.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.categories.length == 0) {
        this.getCategories();
      }
    }
  }
}

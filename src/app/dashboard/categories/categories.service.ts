import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;



@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  baseURL: String;
  headers: any;

  constructor(public http: HttpClient, private storage:Storage) { }
  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });

  }
  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("maderera-dash-jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }
  category: any;
  resultCategory: any;
  profileImg: any;
  get_Category() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "category", { headers: this.headers })
          .subscribe(data => {
            this.resultCategory = data;
            if (this.resultCategory.length > 0) {
              this.category = this.resultCategory[0];
              /*this.getProfileImg().then(() => {
                this.user.pic = this.profileImg[0].pic;
                resolve(this.user);
              });*/
              this.category.pic = "";
              resolve(this.category);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultCategory);
              });
            }
          });
      });
    });
  }
  getProfileImg() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(
            API_REFLECT_URL +
              `picture?filters=sys_user_id:${this.category.id}&sort=id&order=DESC&limit=1`,
            { headers: this.headers }
          )
          .subscribe(data => {
            this.profileImg = data;
            if (
              typeof this.profileImg.status !== "undefined" &&
              this.profileImg.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.profileImg);
              });
            } else {
              resolve(this.profileImg);
            }
          });
      });
    });
  }
  responseSetImg: any;
  setProfileImg(_base64Img) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(
            API_REFLECT_URL + `/picture`,
            { pic: _base64Img, sys_user_id: this.category.id },
            { headers: this.headers }
          )
          .subscribe(data => {
            this.responseSetImg = data;
            if (
              typeof this.responseSetImg.status !== "undefined" &&
              this.responseSetImg.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.responseSetImg);
              });
            } else {
              resolve(this.responseSetImg);
            }
          });
      });
    });
  }

  // -------------------------- Local DB ------------------------- //

  categories: any;
  get_categories(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `category/category-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.categories = data;
            if (
              typeof this.categories.status !== "undefined" &&
              this.categories.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.categories);
              });
            } else {
              resolve(this.categories);
            }
          });
      });
    });
  }
  groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }
  
  get_Categories() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `categories`, { headers: this.headers })
          .subscribe(data => {
            this.categories = data;
            if (
              typeof this.categories.status !== "undefined" &&
              this.categories.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.categories);
              });
            } else {
              resolve(this.categories);
            }
          });
      });
    });
  }

   
   get_CategoriesGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}getallusers.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.categories = data;
            if (
              typeof this.categories.status !== "undefined" &&
              this.categories.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.categories);
              });
            } else {
              resolve(this.categories);
            }
          });
      });
    });

  }

  
  getACategories(categoryId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `user?filters=a.id:${categoryId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.category = data;
            if (
              typeof this.category.status !== "undefined" &&
              this.category.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.category);
              });
            } else {
              resolve(this.category);
            }
          });
      });
    });
  }
  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  addCategory(user: any): any {

    let url = API_REFLECT_URL + "create_category.php";

    let dataUser = JSON.stringify(user);
    console.log(user);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataUser, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
           
            resolve(response);
          }
        });
      });
    });
  }
  deleteCategory(user: any): any {
    let url = API_REFLECT_URL + "delete_category.php";
    console.log(user);
    user.active = 0;
    let data = user
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  updateCategory(user: any) {
    let url = API_REFLECT_URL + "update_category.php";
    let data = JSON.stringify(user);
    let response: any;
    console.log(user);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}
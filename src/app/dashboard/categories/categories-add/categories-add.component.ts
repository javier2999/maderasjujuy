import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
//dimport { Profiler } from 'inspector';
import { CategoriesService } from '../categories.service';
import { NavParams, LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-categories-add',
  templateUrl: './categories-add.component.html',
  styleUrls: ['./categories-add.component.scss'],
})
export class CategoriesAddComponent implements OnInit {

  @Input() categoryPar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;

  loader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  category:any={
  id:null,
  description: "",
  active:0,
  }
  categoryForm:FormGroup;
  validation_messages:any;

  constructor(
    private fb:FormBuilder,
    private service:CategoriesService,
    private navParams:NavParams,
    public loadingController:LoadingController,
    public toastController:ToastController
  )
   {
    this.category=this.navParams.get("categoryPar");
    this.onSave=this.navParams.get("onSaver");
    this.onCancel=this.navParams.get("onCancel");
    this.idx=this.navParams.get("idx");
    this.inputGroup=
    typeof this.category["category-group"] !== "undefined"&&
    this.category["category-group"] !==null
    ? this.category["category-group"][0].group[0].id
    :1;
    this.createForm();
    this.getGroups();
   }

   ngOnInit() {}

   createForm() {
    this.validation_messages = {
      description: [
        {
          type: "minlength",
          message: "La categoria debe tener mas de 3 caracteres."
        },
        { type: "required", message: "La categoria es obligatorio." }
      ],
     
      
    };
    this.categoryForm = this.fb.group({
      id: this.category.id,
      dni: new FormControl(
        this.category.dni,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,9}$")
        ])
      ),
      description: new FormControl(
        this.category.description,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
    
      
      active: this.category.active,
     
    });
  }

  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando categorias..."
  });
  await loader.present();
    this.service
      .getGroups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.category.is_admin = 1;
    }
    this.category.sys_group_id = groupId;
    this.category.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  
  save() {
     
    let profile = this.categoryForm.value;

    
    
    if (typeof profile.id !== "undefined" && profile.id !== null) {
      this.service
        .updateCategory(profile)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Categoria Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addCategory(profile)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear la categoria", "danger");
          } else {
            console.log(data);
            profile.id = this.dataStatus.insertId;
            this.presentToast("Categoria Creada", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(profile, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  } 
  cancel() {
   
    this.onCancel();
  }
}
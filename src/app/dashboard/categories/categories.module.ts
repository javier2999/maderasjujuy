import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImageCropperComponent } from 'ngx-image-cropper';
import { Router, RouterModule, Routes } from '@angular/router';
import { CategoriesPage } from './categories-list/categories.page';
import { CategoriesAddComponent } from './categories-add/categories-add.component';

const routes: Routes = [
  {
    path: "",
    component: CategoriesPage
  },
  {
    path: "add",
    component: CategoriesAddComponent
  }
];

@NgModule({
  declarations: [
    CategoriesPage,
    CategoriesAddComponent,   
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    //ImageCropperComponent,
    RouterModule.forChild(routes)
  ],
  
})
export class CategoriesPageModule { }

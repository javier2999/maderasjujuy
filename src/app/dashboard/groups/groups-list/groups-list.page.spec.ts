import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GroupsListPage } from './groups-list.page';

describe('GroupsListPage', () => {
  let component: GroupsListPage;
  let fixture: ComponentFixture<GroupsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GroupsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

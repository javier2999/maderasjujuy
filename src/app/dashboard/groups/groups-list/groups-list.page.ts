import { Component, OnInit, ViewChild } from '@angular/core';
import { GroupsService } from '../groups.service';
import { AlertController, ModalController } from '@ionic/angular';
import { ExcelServiceProvider } from 'src/app/services/excelService/excel-service.service';
import { ScreenService } from 'src/app/app-screen.service';
import { GroupsAddComponent } from '../groups-add/groups-add.component';

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.page.html',
  styleUrls: ['./groups-list.page.scss'],
})
export class GroupsListPage implements OnInit {

  @ViewChild("inputSearch",{static:true}) inputSearch;
   groups:any;
   groupsSinFiltro:any;
   isLoading=false;
   searchGroup:Object={group:"",offset:0, limit:200};
   global_vars:any;
   infiniteRow:number=100;
   hide_sm;
   group:string="";
   dataStatus:any;
   editModal:any;
   screen:any;

  constructor(
    private service:GroupsService,
    public modalController:ModalController,
    public alertcontroller:AlertController,
    public exelservice:ExcelServiceProvider,
    private screenService:ScreenService
  ) { 
    screenService.screen.subscribe(_screen =>{
      this.screen=_screen;
    });
    this.getGroups();
    this.hide_sm=false;
  }

  ngOnInit() {
  }
  getGroups(){

    this.searchGroup["group"] = this.group;
    this.searchGroup["offset"] = 0;
    this.searchGroup["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getgroupGroup(this.searchGroup)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.groups);
          this.groupsSinFiltro = this.dataStatus;
          
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;

   }
   async edit(groupIndex: any) {
    let group = this.groups[groupIndex];
    this.editModal = await this.modalController.create({
      component: GroupsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        groupPar: group,
        idx: groupIndex,
        onSave: this.saveGroupEdited,
        onCancel: this.cancelGroupEdition
      }
    });
    return await this.editModal.present();
  }
  async addGroup() {
    let groupPar = {
      id: null,
      description: "",
      active: 1,
      "group-group": null


    };
    this.editModal = await this.modalController.create({
      component: GroupsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        groupPar: groupPar,
        idx: null,
        onSave: this.addNewGroup,
        onCancel: this.cancelGroupEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.exelservice.exportAsExcelFile(this.groups, "groups");
  }
  saveGroupEdited = (groupInfo: any, groupIndex: any) => {
    this.groups[groupIndex] = groupInfo;
    this.editModal.dismiss();
  };
  addNewGroup = (groupInfo: any, groupIndex: any) => {
    this.groups.push(groupInfo);
    this.editModal.dismiss();
  };
  cancelGroupEdition = () => {
    this.editModal.dismiss();
  };
  async delete(groupIndex: any) {
    const alert = await this.alertcontroller.create({
      header: "Quitar Grupo",
      message: "¿Confirma que quiere borrar el Empleado?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let group = this.groups[groupIndex];
            let par = { id: group.id, active: 0 };

            this.service
              .deleteGroup(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getGroups();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }
  initializeItems() {
    this.groups = this.groupsSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

   
    const val = ev.target.value;

    
    if (val && val.trim() != "") {
      this.groups = this.groups.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.groups.length == 0) {
        this.getGroups();
      }
    }
  }

}


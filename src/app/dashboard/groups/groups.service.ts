import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';



const API_AUTH_URL=`${environment.API_PATH}auth`;
const API_REGLECT_URL=`${environment.API_PATH}`;

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
baseURL:string;
headers:any;
  constructor(public http:HttpClient, private storage:Storage) { }
  set_headers(){
    return new Promise(resolve=>{
    this.storage.get("jtw").then(val=>{
      this.headers=new HttpHeaders({
        "content-type":"aplication/json",
        Accept:"aplication/json",
        Authorization:"Bearer"+val
      });
      resolve();
    });
    console.log(this.storage);
    })
  }
 close:any;
 closeSession(){
   return new Promise(resolve=>{
     this.set_headers().then(()=>{
       this.http
       .post(API_AUTH_URL+`logout`,{},{headers:this.headers})
       .subscribe(data=>{
         this.storage.remove("maderera-dash-jwt");
         this.close=data;
         resolve(this.close)
       });
     });
   });
 }
 group:any;
 resutGroup:any;
 profileImg:any;
getGroup(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
    this.http
    .get(API_AUTH_URL+"group",{headers:this.headers})
    .subscribe(data=>{
      this.resutGroup=data;
      if(this.resutGroup.length>0){
      this.group=this.resutGroup[0];
      this.group.pic="";
      resolve(this.group);}
      else{
        this.closeSession().then(()=>{
          resolve(this.resutGroup);
          });
        }
      });
    });
  });
 };
 getProfileImg(){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .get(
        API_REGLECT_URL + 
        `picture?filters=group:${this.group.id}&sort=id&order=Des&limit=1`,
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.profileImg=data;
        if(
          typeof this.profileImg.status !=="undefined"&&
          this.profileImg.status==false
        ){
          this.closeSession().then(()=>{
            resolve(this.profileImg);
          });
        }else{
          resolve(this.profileImg);
        }
      });
    });
  });
}
responseSetImg:any;
setProfileImg(_base64Img){
  return new Promise(resolve=>{
    this.set_headers().then(()=>{
      this.http
      .post(
        API_REGLECT_URL+`/picture`,
        {pic: _base64Img, sys_group_id:this.group.id},
        {headers:this.headers}
      )
      .subscribe(data=>{
        this.responseSetImg=data;
        if(
          typeof this.responseSetImg.status !=="undefined" &&
          this.responseSetImg.status==false
        ) {
          this.closeSession().then(()=>{
            resolve(this.responseSetImg);
          });
        } else{
          resolve(this.responseSetImg);
        }
      });
    });
  });
}
// -------------------------- Local DB ------------------------- //
grupos: any;
getGrupo(param) {
  return new Promise(resolve => {
    this.set_headers().then(() => {
      this.http
        .get(API_REGLECT_URL + `group/group-group/group`, {
          headers: this.headers
        })
        .subscribe(data => {
          this.grupos = data;
          if (
            typeof this.grupos.status !== "undefined" &&
            this.grupos.status == false
          ) {
            this.closeSession().then(() => {
              resolve(this.grupos);
            });
          } else {
            resolve(this.grupos);
          }
        });
    });
  });
}
  _groups: any;
  get_Groups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REGLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this._groups = data;
            if (
              typeof this._groups.status !== "undefined" &&
              this._groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this._groups);
              });
            } else {
              resolve(this._groups);
            }
          });
      });
    });
  }
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REGLECT_URL + `group`, { headers: this.headers })
          .subscribe(data => {
            this.grupos = data;
            if (
              typeof this.grupos.status !== "undefined" &&
              this.grupos.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.grupos);
              });
            } else {
              resolve(this.grupos);
            }
          });
      });
    });
  }
  getgroupGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(`${API_AUTH_URL}getallgroup.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.grupos = data;
            if (
              typeof this.grupos.status !== "undefined" &&
              this.grupos.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.grupos);
              });
            } else {
              resolve(this.grupos);
            }
          });
      });
    });
  }
  getGRupos(groupId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + `group?filters=a.id:${groupId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.group = data;
            if (
              typeof this.group.status !== "undefined" &&
              this.group.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.group);
              });
            } else {
              resolve(this.group);
            }
          });
      });
    });
  }
  permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }
  addGroup(group: any): any {

    let url = API_REGLECT_URL + "create_group.php";

    let dataGroup = JSON.stringify(group);
    console.log(group);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataGroup, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
      
            resolve(response);
          }
        });
      });
    });
  }
  deleteGroup(group: any): any {
    let url = API_REGLECT_URL + "delete_group.php";
    console.log(group);
    group.active = 0;
    let data = group
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }
  updateGroup(group: any) {
    let url = API_REGLECT_URL + "update_group.php";
    let data = JSON.stringify(group);
    let response: any;
    console.log(group);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GroupsListPage } from './groups-list/groups-list.page';
import { GroupsAddComponent } from './groups-add/groups-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImageCropperComponent } from 'ngx-image-cropper';

const routes: Routes=[
  {
path:"",
component:GroupsListPage
  },
  {
    path:"add",
    component:GroupsAddComponent
  }
];

@NgModule({
  declarations: [
    GroupsListPage,
    GroupsAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
   // ImageCropperComponent,
    RouterModule.forChild(routes)
  ]
})
export class GroupspageModule { }

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, NavParams, ToastController } from '@ionic/angular';
import { GroupsService } from '../groups.service';

@Component({
  selector: 'app-groups-add',
  templateUrl: './groups-add.component.html',
  styleUrls: ['./groups-add.component.scss'],
})
export class GroupsAddComponent{
  @Input() groupPar:any;
  @Input() idx:any;
  @Input() onSave:any;
  @Input() onCancel:any;
  oader:any;
  dataStatus:any;
  hide_Sm:any;
  inBrowser:Boolean=false;
  inputGroup: number=0;
  messaje:string="";
  class:string="alert d-none";
  groups:any;
  selectedGroup:number;
  group:any={
  id:null,
  description: "",
  first_name: "",
  active:0,


  };
  groupForm:FormGroup;
  validation_messages:any;
  constructor(
    private fb:FormBuilder,
    private service:GroupsService,
    private navParams:NavParams,
    public loadingController:LoadingController,
    public toastController:ToastController
  ) {
     this.group=this.navParams.get("groupPar");
     this.onSave=this.navParams.get("onSave");
     this.onCancel=this.navParams.get("onCancel");
     this.idx=this.navParams.get("idx");
     this.inputGroup=
     typeof this.group["group-group"]  !== "undefined"&&
     this.group["group-group"] !==null
     ? this.group["group-group"][0].group[0].id
     :1;
     this.createForm();
     this.getGroups();
    
   }
   createForm() {
    this.validation_messages = {
    
      description: [
        {
          type: "minlength",
          message: "El grupo debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El grupo es obligatorio." }
      ]
     
      
    };
    this.groupForm = this.fb.group({
      id: this.group.id,
      
      description: new FormControl(
        this.group.description,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      
      active: this.group.active,
     
    });
  }
  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .getGroups()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }
  setGroup(groupId: number) {
    if (groupId == 3) {
      this.group.is_admin = 1;
    }
    this.group.sys_group_id = groupId;
    this.group.sys_group_id = 1;
    this.selectedGroup = groupId;
  }
  save() {
     
    let group = this.groupForm.value;

    
    
    if (typeof group.id !== "undefined" && group.id !== null) {
      this.group
        .updateGroup(group)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Empleado Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addGroup(group)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el usuario", "danger");
          } else {
            console.log(data);
            group.id = this.dataStatus.insertId;
            this.presentToast("Usuario Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(group, this.idx);
  }
  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });
    toast.present();
  }
  cancel() {
   
    this.onCancel();
  }

}

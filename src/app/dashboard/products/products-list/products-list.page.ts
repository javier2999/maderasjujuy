import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { ProductsService } from "../products.service";
import { ProductsAddComponent } from "../products-add/products-add.component";
import { ProductsFracComponent } from "../products-frac/products-frac.component";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.page.html',
  styleUrls: ['./products-list.page.scss'],
})
export class ProductsListPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  products: any;
  productsSinFiltro: any;
  isLoading = false;
  searchProduct: Object = { product: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  product: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: ProductsService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService
  ) {
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });

    this.getProducts();

    this.hide_sm = false;
  }

  ngOnInit() {
    //this.getProducts();
  }

  getProducts() {
    this.searchProduct = this.product;
    console.log()
    this.isLoading = true;
    this.service
      .getProducts()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.products = this.dataStatus.products;
          console.log(this.products);
          this.productsSinFiltro = this.dataStatus;
          //console.log(this.products);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;
  }

  async edit(productIndex: any) {
    let product = this.products[productIndex];
    this.editModal = await this.modalController.create({
      component: ProductsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        productPar: product,
        idx: productIndex,
        onSave: this.saveProductEdited,
        onCancel: this.cancelProductEdition
      }
    });
    return await this.editModal.present();
  }

  async fraccionar(productIndex: any) {
    let product = this.products[productIndex];
    this.editModal = await this.modalController.create({
      component: ProductsFracComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        productPar: product,
        idx: productIndex,
        onSave: this.saveProductFraccionado,
        onCancel: this.cancelProductEdition
      }
    });
    return await this.editModal.present();
  }

  async addProduct() {
    let productPar = {
      id: null,
      dni: "",
      last_name: "",
      first_name: "",
      phone: "",
      email: "",
      password: "",
      active: 1,
      is_admin: 1,
      sys_tipo_id: 1,
      "product-tipo": null
    };
    this.editModal = await this.modalController.create({
      component: ProductsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        productPar: productPar,
        idx: null,
        onSave: this.addNewProduct,
        onCancel: this.cancelProductEdition
      }
    });
    return await this.editModal.present();
  }

  downloadExcel() {
    this.excelService.exportAsExcelFile(this.products, "products");
  }

  saveProductEdited = (productInfo: any, productIndex: any) => {
    this.products[productIndex] = productInfo;
    this.editModal.dismiss();
  };

  saveProductFraccionado = (productInfo: any, productIndex: any) => {
    this.products[productIndex] = productInfo;
    this.editModal.dismiss();
  };

  addNewProduct = (productInfo: any, productIndex: any) => {
    this.products.push(productInfo);
    this.editModal.dismiss();
  };

  cancelProductEdition = () => {
    this.editModal.dismiss();
  };

  async delete(productIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Producto",
      message: "¿Confirma que quiere borrar el producto?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let product = this.products[productIndex];
            let par = { id: product.id, active: 0 };

            this.service
              .deleteProduct(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getProducts();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }

  initializeItems() {
    this.products = this.productsSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getProducts();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.products = this.products.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.products.length == 0) {
        this.getProducts();
      }
    }
  }
}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ProductsListPage } from "./products-list/products-list.page";
import { ProductsAddComponent } from "./products-add/products-add.component";
import { ImageCropperModule } from "ngx-image-cropper";
//import { DriversComponent } from "./products-drivers/drivers/drivers.component";

const routes: Routes = [
  {
    path: "",
    component: ProductsListPage
  },
  {
    path: "add",
    component: ProductsAddComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProductsListPage,
    ProductsAddComponent,
  ],
})
export class ProductsPageModule {}

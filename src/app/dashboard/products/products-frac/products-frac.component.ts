import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import { NavParams, LoadingController, ToastController } from "@ionic/angular";
import { ProductsService } from "../products.service";

import { ProductsListPage } from "../products-list/products-list.page";

@Component({
  selector: "page-products-frac",
  templateUrl: "products-frac.component.html"
})
export class ProductsFracComponent {
  @Input() productPar: any;
  @Input() idx: any;
  @Input() onSave: any;
  @Input() onCancel: any;

  loader: any;
  dataStatus: any;
  hide_sm: any;
  isBrowser: Boolean = false;
  inputGroup: number = 0;
  messaje: string = "";
  class: string = "alert d-none";
  groups: any;
  selectedGroup: number;
  product: any = {
    id: null,
    nick: "",
    email: "",
    password: "",
    active: 0,
    is_admin: 0,
    sys_group_id: 0,
    encryptPassword: false
  };
  productForm: FormGroup;
  validation_messages: any;

  constructor(
    private fb: FormBuilder,
    private service: ProductsService,
    private navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {
    /*     if (this.platform.is('mobileweb') || this.platform.is('core')) {
          this.isBrowser = true;
      }
      this.hide_sm = platform.width() < 576; */
    this.product = this.navParams.get("productPar");
    this.onSave = this.navParams.get("onSave");
    this.onCancel = this.navParams.get("onCancel");
    this.idx = this.navParams.get("idx");
    this.inputGroup =
      typeof this.product["product-group"] !== "undefined" &&
      this.product["product-group"] !== null
        ? this.product["product-group"][0].group[0].id
        : 1;
    this.createForm();
    this.getGroups();
  }
  createForm() {
    this.validation_messages = {
      dni: [
        { type: "required", message: "El DNI es obligatorio." },
        {
          type: "pattern",
          message:
            "El DNI debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      nick: [
        {
          type: "minlength",
          message: "El nombre debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El nombre es obligatorio." }
      ],
      email: [
        {
          type: "pattern",
          message: "El email no coincide con un formato válido."
        },
        { type: "required", message: "El email es obligatorio." },
        { type: "validEmail", message: "El email ya existe o no es válido." }
      ],
      productname: [
        { type: "required", message: "Username is required." },
        {
          type: "minlength",
          message: "La contraseña debe tener mas de 5 caracteres."
        },
        {
          type: "maxlength",
          message: "La contraseña debe tener menos de 25 caracteres."
        },
        {
          type: "pattern",
          message:
            "La contraseña debe estar compuesta solo por numeros y letras."
        }
      ]
    };
    this.productForm = this.fb.group({
      id: this.product.id,
      dni: new FormControl(
        this.product.dni,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,9}$")
        ])
      ),
      nick: new FormControl(
        this.product.nick,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      phone: this.product.phone,
      password: new FormControl(
        this.product.password,
        Validators.compose([
          Validators.maxLength(25),
          Validators.minLength(5),
          Validators.pattern("^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$"),
          Validators.required
        ])
      ),
      active: this.product.active,
      is_admin: this.product.is_admin,
      sys_group_id: this.inputGroup
    });
  }

  async getGroups() {
    let loader = await this.loadingController.create({
      message: "Buscando Grupos..."
    });
    await loader.present();
    this.service
      .getTipos()
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //Login
        } else {
          this.groups = this.dataStatus.groups;
          console.log(this.dataStatus);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }

  setGroup(groupId: number) {
    if (groupId == 3) {
      this.product.is_admin = 1;
    }
    this.product.sys_group_id = groupId;
    this.product.sys_group_id = 1;
    this.selectedGroup = groupId;
  }

  save() {
    //this.presentToast("Guardandoo...", "alert alert-secondary");
    //this.product.id = null;
    //this.validate_sys_product((response) => response.JSON());
    let product = this.productForm.value;

    if (this.product.password !== this.productForm.value.password) {
      product.encryptPassword = true;
    }
    if (typeof product.id !== "undefined" && product.id !== null) {
      this.service
        .updateProduct(product)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            //Login
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Usuario Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addProduct(product)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el usuario", "danger");
          } else {
            console.log(data);
            product.id = this.dataStatus.insertId;
            this.presentToast("Usuario Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(product, this.idx);
  }

  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  cancel() {
    //this.navCtrl.setRoot(UsuariosPage, {}, { animate: true, direction: 'forward' });
    //this.router.navigate(['dashboard/products']);
    this.onCancel();
  }
}

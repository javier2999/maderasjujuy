import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  baseURL: String;
  headers: any;
  productos: any;
  product: any;
  products: any;
  resultProduct: any;
  constructor(public http: HttpClient, private storage: Storage) {}

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  getProducts() {
    return new Promise(resolve => {
      var a = {codbar: "true"};
      let dataProduct = JSON.stringify(a);
      console.log(dataProduct);
      this.set_headers().then(() => {
        this.http
          .post(API_REFLECT_URL + `get_products.php`, dataProduct,{ headers: this.headers })
          .subscribe(data => {
            this.products = data;
            if (
              typeof this.products.status !== "undefined" &&
              this.products.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.products);
              });
            } else {
              resolve(this.products);
            }
          });
      });
    });
  }

  getAProduct(productId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `product?filters=a.id:${productId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.product = data;
            if (
              typeof this.product.status !== "undefined" &&
              this.product.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.product);
              });
            } else {
              resolve(this.product);
            }
          });
      });
    });
  }

  addProduct(product: any): any {

    let url = API_REFLECT_URL + "create_product.php";

    let dataProduct = JSON.stringify(product);
    console.log(product);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataProduct, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "product_id": response.insertId,
                "newPassword": product.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.product.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  deleteProduct(product: any): any {
    let url = API_REFLECT_URL + "delete_product.php";
    console.log(product);
    product.active = 0;
    let data = product
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateProduct(product: any) {
    let url = API_REFLECT_URL + "update_product.php";
    let data = JSON.stringify(product);
    let response: any;
    console.log(product);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }

  tipos: any;
  getTipos() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}get_productos_tipos.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.tipos = data;
            if (
              typeof this.tipos.status !== "undefined" &&
              this.tipos.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.tipos);
              });
            } else {
              resolve(this.tipos);
            }
          });
      });
    });
  }


  unidades: any;
  getUnidades() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}get_productos_unidades.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.unidades = data;
            if (
              typeof this.unidades.status !== "undefined" &&
              this.unidades.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.unidades);
              });
            } else {
              resolve(this.unidades);
            }
          });
      });
    });
  }




}
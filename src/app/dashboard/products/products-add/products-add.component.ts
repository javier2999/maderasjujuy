import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import { NavParams, LoadingController, ToastController } from "@ionic/angular";
import { ProductsService } from "../products.service";

import { ProductsListPage } from "../products-list/products-list.page";

@Component({
  selector: "page-products-add",
  templateUrl: "products-add.component.html"
})
export class ProductsAddComponent {
  @Input() productPar: any;
  @Input() idx: any;
  @Input() onSave: any;
  @Input() onCancel: any;

  loader: any;
  dataStatusTipos: any;
  dataStatusProduct:any;
  dataStatusUnidades: any;
  hide_sm: any;
  isBrowser: Boolean = false;
  inputTipo: number = 0;
  inputUnidad: number = 0;
  messaje: string = "";
  class: string = "alert d-none";
  tipos: any;
  unidades: any;
  selectedTipo: number;
  selectedUnidad: number;
  product: any = {
    id: null,
    descripo: "",
    codbar: "",
    password: "",
    cost_price: 0,
    sell_price: 0,
    stock: 0,
    active: 0,
    sys_tipo_id: 0,
    sys_unidad_id: 0,
  };
  productForm: FormGroup;
  validation_messages: any;

  constructor(
    private fb: FormBuilder,
    private service: ProductsService,
    private navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {

    this.product = this.navParams.get("productPar");
    this.onSave = this.navParams.get("onSave");
    this.onCancel = this.navParams.get("onCancel");
    this.idx = this.navParams.get("idx");
    this.inputTipo =
      typeof this.product["product-tipo"] !== "undefined" &&
      this.product["product-tipo"] !== null
        ? this.product["product-tipo"][0].tipo[0].id
        : 1;
    this.inputUnidad =
      typeof this.product["product-unidad"] !== "undefined" &&
      this.product["product-unidad"] !== null
        ? this.product["product-unidad"][0].unidad[0].id
        : 1;        
    this.createForm();
    this.getTipos();
    this.getUnidades();
  }

  createForm() {
    this.validation_messages = {
      cost_price: [
        { type: "required", message: "El DNI es obligatorio." },
        {
          type: "pattern",
          message:
            "El DNI debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      sell_price: [
        { type: "required", message: "El DNI es obligatorio." },
        {
          type: "pattern",
          message:
            "El DNI debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      codbar: [
        {
          type: "pattern",
          message: "El email no coincide con un formato válido."
        },
        { type: "required", message: "El email es obligatorio." },
        { type: "validCodbar", message: "El codigo de barras ya existe o no es válido." }
      ],
      unidad: [
        {
          type: "pattern",
          message: "La unidad no coincide con un formato válido."
        },
        { type: "required", message: "La unidad es obligatorio." },
        { type: "validUnidad", message: "El codigo de barras ya existe o no es válido." }
      ],
      descrip: [
        { type: "required", message: "El nombre del producto es obligatorio." },

      ],
      tipo: [
        { type: "required", message: "El nombre del producto es obligatorio." },

      ],
      stock: [
        { type: "required", message: "El nombre del producto es obligatorio." },

      ],
      active: [
        { type: "required", message: "El nombre del producto es obligatorio." },

      ]
    };
    this.productForm = this.fb.group({
      id: this.product.id,
      descrip: new FormControl(
        this.product.descrip,
        Validators.compose([
          Validators.required
        ])
      ),
      codbar: new FormControl(
        this.product.codbar,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      unidad: new FormControl(
        this.product.unidad,
        Validators.compose([
          Validators.required
        ])
      ),
      cost_price: parseFloat(this.product.cost_price),
      sell_price: parseFloat(this.product.sell_price),
      active: this.product.active,
      unityid: this.inputUnidad,
      tipprodid: this.inputTipo,
      stock: this.product.stock,
      sys_unidad_id: this.inputUnidad,
      sys_tipo_id: this.inputTipo
    });
  }

  async getTipos() {
    let loader = await this.loadingController.create({
      message: "Buscando Tipos de productos..."
    });
    await loader.present();
    this.service
      .getTipos()
      .then(data => {
        this.dataStatusTipos = data;
        if (
          typeof this.dataStatusTipos.status !== "undefined" &&
          this.dataStatusTipos.status == false
        ) {
          //Login
        } else {
          this.tipos = this.dataStatusTipos.tipos;
          console.log(this.dataStatusTipos);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }

  async getUnidades() {
    let loader = await this.loadingController.create({
      message: "Buscando Unidades de productos..."
    });
    await loader.present();
    this.service
      .getUnidades()
      .then(data => {
        this.dataStatusUnidades = data;
        if (
          typeof this.dataStatusUnidades.status !== "undefined" &&
          this.dataStatusUnidades.status == false
        ) {
          //Login
        } else {
          this.unidades = this.dataStatusUnidades.unidades;
          console.log(this.dataStatusUnidades);
        }
      })
      .catch(err => {
        console.log(err);
      });

    await loader.dismiss();
  }

  setTipo(tipoId: number) {
    this.product.sys_tipo_id = tipoId;
    this.product.tipprodId = tipoId;
    this.selectedTipo = tipoId;
  }

  setUnidad(unidadId: number) {
    this.product.sys_unidad_id = unidadId;
    this.product.unityid = unidadId;
    this.selectedUnidad = unidadId;
  }

  save() {
    let product = this.productForm.value;
    if (typeof product.id !== "undefined" && product.id !== null) {
      this.service
        .updateProduct(product)
        .then(data => {
          this.dataStatusProduct = data;
          if (
            typeof this.dataStatusProduct.status !== "undefined" &&
            this.dataStatusProduct.status == false
          ) {
            //Login
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Usuario Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addProduct(product)
        .then(data => {
          this.dataStatusProduct = data;
          if (
            typeof this.dataStatusProduct.status !== "undefined" &&
            this.dataStatusProduct.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el producto", "danger");
          } else {
            console.log(data);
            product.id = this.dataStatusProduct.insertId;
            this.presentToast("Producto creado con éxito", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(product, this.idx);
  }

  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  cancel() {
    //this.navCtrl.setRoot(UsuariosPage, {}, { animate: true, direction: 'forward' });
    //this.router.navigate(['dashboard/products']);
    this.onCancel();
  }
}

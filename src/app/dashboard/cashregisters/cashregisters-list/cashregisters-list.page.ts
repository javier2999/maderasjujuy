import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { CashregistersService } from "../cashregisters.service";
//import { ClientsAddComponent } from "../clients-add/clients-add.component";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";

@Component({
  selector: 'app-cashregisters-list',
  templateUrl: './cashregisters-list.page.html',
  styleUrls: ['./cashregisters-list.page.scss'],
})
export class CashregistersPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  cashregisters: any;
  cashregistersSinFiltro: any;
  isLoading = false;
  searchCashregister: Object = { cashregister: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  cashregister: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: CashregistersService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService
  ) {
      screenService.screen.subscribe(_screen => {
        this.screen = _screen;
    });

    this.getCashregisters();

    this.hide_sm = false;
  }

  ngOnInit() {}

  getCashregisters() {
    this.searchCashregister["cashregister"] = this.cashregister;
    this.searchCashregister["offset"] = 0;
    this.searchCashregister["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getCashregistersGroup(this.searchCashregister)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.cashregisters = this.dataStatus.cashregisters;
          console.log(this.cashregisters);
          this.cashregistersSinFiltro = this.dataStatus;
          //console.log(this.users);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;
  }

  /*async edit(clientIndex: any) {
    let client = this.clients[clientIndex];
    this.editModal = await this.modalController.create({
      component: ClientsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        clientPar: client,
        idx: clientIndex,
        onSave: this.saveClientEdited,
        onCancel: this.cancelClientEdition
      }
    });
    return await this.editModal.present();
  }*/

  /*async addClient() {
    let clientPar = {
      id: null,
      apynom: "",
      landline: "",
      phone: "",
      dni: "",
      purchase_margin: "",
      iva_category: "",
      address: "",
      client_typeId: null,
      discount: "",
      active: 1,
    };
    this.editModal = await this.modalController.create({
      component: ClientsAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        clientPar: clientPar,
        idx: null,
        onSave: this.addNewClient,
        onCancel: this.cancelClientEdition
      }
    });
    return await this.editModal.present();
  }*/

  downloadExcel() {
    this.excelService.exportAsExcelFile(this.cashregisters, "cashregisters");
  }

  /*saveClientEdited = (clientInfo: any, clientIndex: any) => {
    this.clients[clientIndex] = clientInfo;
    this.editModal.dismiss();
  };

  addNewClient = (clientInfo: any, clientIndex: any) => {
    this.clients.push(clientInfo);
    this.editModal.dismiss();
  };

  cancelClientEdition = () => {
    this.editModal.dismiss();
  };*/

  /*async delete(clientIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Cliente",
      message: "¿Confirma que quiere borrar el cliente?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let client = this.clients[clientIndex];
            let par = { id: client.id, active: 0 };

            this.service
              .deleteClient(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getClients();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }*/

  initializeItems() {
    this.cashregisters = this.cashregistersSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getUsers();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.cashregisters = this.cashregisters.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.cashregisters.length == 0) {
        this.getCashregisters();
      }
    }
  }
}




import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { BehaviorSubject, forkJoin, Observable, of, throwError } from "rxjs";
import { environment } from "../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}`;
const TOKEN_KEY = "jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authentication = new BehaviorSubject({
    state: true,
    initials: "MD",
    firstName: "Matias",
    lastName: "Dilascio"
  });

  headers: any;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private plt: Platform
  ) {
      this.plt.ready().then(() => {
          this.checkToken();
      });
   }

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authentication.next({
          state: true,
          initials: "SS",
          firstName: "Sinap",
          lastName: "Sys"
        });
      }
    });
  }

  loginResponse: any;
  login(user: string, password: string, callback: (data) => void) {
    console.log("safa");
    console.log({user,password});
    var a = '{"user":'+user+', "password":'+password+'}';
    this.http
      .post(`${API_AUTH_URL}login.php`, { user, password })
      .subscribe(data => {
        this.loginResponse = data;
        console.log(data);
        
        console.log(this.loginResponse);
        if (this.loginResponse.status) {
          this.storage.set(TOKEN_KEY, this.loginResponse.jwt).then(() => {
            this.set_headers().then(() => {
              this.storage.set("DASH_USER", this.loginResponse.first_name).then(() => {});
              this.authentication.next({
                state: true,
                initials: "SS",
                firstName: "Sinap",
                lastName: "Sys"
              });
              callback(true);
            });
          });
        } else {
          callback(false);
        }
      });
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authentication.next({
        state: false,
        initials: "",
        firstName: "",
        lastName: ""
      });
    });
  }

  isAuthenticated() {
    return this.authentication.value.state;
  }

  forgotPassword(email: string, callback: any) {
    let header = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    });
    this.http.get(`${API_AUTH_URL}resetPassword`).subscribe(data => {
      this.loginResponse = data;
      if (this.loginResponse.status) {
        callback(true);
      } else {
        callback(false);
      }
    });
  }
}

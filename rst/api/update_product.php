<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/product.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$product = new Product($db);
 
$data  =file_get_contents("php://input");
// get posted data
$data = json_decode(file_get_contents("php://input"));
$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $nombre => $valor) {
	if(substr($nombre,0,13) == 'Authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
 
// if jwt is not empty
if($jwt2){
 
    // if decode succeed, show product details
    try {
 
        // decode jwt
        $decoded = JWT::decode($jwt2, $key, array('HS256'));
 
		// set product property values
		$product->id = $data->id;
		$product->codbar = $data->codbar;
		$product->descrip = $data->descrip;
		$product->cost_price = $data->cost_price;
		$product->sell_price = $data->sell_price;
		$product->stock = $data->stock;
		$product->unityid = $data->unityid;
		$product->tipprodid = $data->tipprodid;
		$product->active = $data->active;

		 
		// update the product record
		if($product->update()){
			// we need to re-generate jwt because product details might be different
			$token = array(
				   "id" => $product->id,
				   "codbar" => $product->codbar,
				   "descrip" => $product->descrip,
				   "cost_price" => $product->cost_price,
				   "sell_price" => $product->sell_price,
				   "tipprodid" => $product->tipprodid,
				   "unityid" => $product->unityid,				   
				   "stock" => $product->stock,
				   "active" => $product->active
			   );
			$jwt2 = JWT::encode($token, $key);
			 
			// set response code
			http_response_code(200);
			 
			// response in json format
			echo json_encode(
					array(
						"message" => "product was updated.",
						"jwt" => $jwt
					)
				);
		}
		 
		// message if unable to update product
		else{
			// set response code
			http_response_code(401);
		 
			// show error message
			echo json_encode(array("message" => "Unable to update product."));
		}
    }
 
    // if decode fails, it means jwt is invalid
	catch (Exception $e){
	 
		// set response code
		http_response_code(401);
	 
		// show error message
		echo json_encode(array(
			"message" => "Access denied.",
			"error" => $e->getMessage()
		));
	}
}
 
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the product access denied
    echo json_encode(array("message" => "Access denied."));
}
?>
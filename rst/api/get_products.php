<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/product.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$product = new Product($db);
 
$postdata=file_get_contents("php://input");
$data = json_decode($postdata);

$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $nombre => $valor) {
	if(substr($nombre,0,13) == 'Authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
 
// if jwt is not empty
if($jwt2){
 
    // if decode succeed, show product details
    try {
 
        // decode jwt
        $decoded = JWT::decode($jwt2, $key, array('HS256'));

		$product->codbar = $data->codbar;


		if($data->codbar == "true")
		{
			$arrayProd = $product->getProductAll();
			// update the product record
			if($arrayProd){
				// we need to re-generate jwt because product details might be different
				$token = array(
					"products" => $arrayProd
				);
				
				$jwt = JWT::encode($token, $key);
				
				// set response code
				http_response_code(200);
				
				// response in json format
				echo json_encode(array("products" => $arrayProd, "status"=> true));
			}
			
			// message if unable to update product
			else{
				// set response code
				http_response_code(401);
			
				// show error message
				echo json_encode(array("products" => array()));
			}
		}
		else
		{
			if(!empty($product->codbar))
			{
				// update the product record
				if($product->getProduct($data->codbar)){
					// we need to re-generate jwt because product details might be different
					$token = array(
						"id" => $product->id,
						"descrip" => $product->descrip,
						"sell_price" => floatval($product->sell_price),
						"active" => $product->active,
						"stock" => $product->stock,
						"quantity"=> 1,
						"status"=> true
					);
					
					$jwt = JWT::encode($token, $key);
					
					// set response code
					http_response_code(200);
					
					// response in json format
					echo json_encode(array($token));
				}
				
				// message if unable to update product
				else{
					// set response code
					http_response_code(401);
				
					// show error message
					echo json_encode(array("message" => "Unable to update product."));
				}
			}
			else{
				// update the product record
				$arrayProdDesc= $product->getProductDescrip($data->descrip);
				//$arrayProd = $product->getProductAll($data->clienteid);
				if($arrayProdDesc){
					// we need to re-generate jwt because product details might be different
					$token = array($arrayProdDesc);
					
					$jwt = JWT::encode($token, $key);
					
					// set response code
					http_response_code(200);
					
					// response in json format
					echo json_encode($arrayProdDesc);
				}
				
				// message if unable to update product
				else{
					// set response code
					http_response_code(401);
				
					// show error message
					echo json_encode(array("message" => "Unable to update getproductdescrip.",
											"data" => $data
							));
				}
			}
		}
    }
 
   // if decode fails, it means jwt is invalid
	catch (Exception $e){
	 
		// set response code
		http_response_code(401);
	 
		// show error message
		echo json_encode(array(
			"message" => "Access denied.",
			"error" => $e->getMessage(),
			"safa"=> $jwt,
			"data"=> $data,
			"status"=> false
		));
	}
}
 
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the product access denied
    echo json_encode(array("message" => $jwt));
}
?>
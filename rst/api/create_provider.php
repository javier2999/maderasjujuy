<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/provider.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate Provedor object
$provider = new provider($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set Provedor property values
$provider-> id = $data->id;
$provider-> company_name = $data->company_name;
$provider-> cuit = $data->cuit;
$provider-> ivacategid = $data->ivacategid;
$provider-> phone = $data->phone;
$provider-> active = $data->active;



$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $company_name => $valor) {
	if(substr($company_name,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
if($jwt2){
    if(!$provider->providerExists())
    {
        if(
            !empty($provider->cuit) &&
            !empty($provider->company_name) &&
            !empty($provider->phone) &&
            !empty($provider->ivacategid) &&
            $provider->create()
        ){
            $token = array(
                "providerid" => 1,
                "respuesta" => "safa",
                "message" => "Provedor was created."
            );
        
            http_response_code(200);
            echo json_encode(array($token));
        }
        else{
            $token = array(
                "providerid" => 1,
                "respuesta" => "safa",
                "message" => "Unable to create Provedor."
            );
            http_response_code(400);
            echo json_encode(array($token));
        }
    }
    else
    {
        $token = array(
            "providerid" => 1,
            "respuesta" => "safa",
            "message" => "Provedor existente"
        );
        http_response_code(400);
        echo json_encode(array($token));
    }
}
else{
        // set response code
        http_response_code(401);
 
        // tell the product access denied
        echo json_encode(array("message" => $jwt2));
}

?>
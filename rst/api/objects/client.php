<?php  
class Client{
private $conn;
<<<<<<< HEAD
private $table_name = "Client";
=======
private $table_name = "cli";
>>>>>>> 44c57e5d222dc2e258bf779484afe3814195a4e6

public $id;
public $first_name;
public $last_name;
public $dni;
public $purchase_margin;
public $ivacategid;
public $address;
public $tipclientid;
public $discount;
public $phone;
public $active;


// constructor
    public function __construct($db){
    $this->conn = $db;
    }

    // create new client record

    function create(){
 
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    first_name = :first_name,
                    last_name = :last_name,
                    dni = :dni,
                    address = :address,
                    purchase_margin = :purchase_margin,
                    phone = :phone,
                    ivacategid = :ivacategid,
                    tipclientid = :tipclientid,
                    discount = :discount,
                    active = 1";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
     
        // bind the values
        $stmt->bindParam(':first_name', $this->first_name);
        $stmt->bindParam(':last_name', $this->last_name);
        $stmt->bindParam(':dni', $this->dni);
        $stmt->bindParam(':address', $this->address);
        $stmt->bindParam(':purchase_margin', $this->purchase_margin);
        $stmt->bindParam(':ivacategid', $this->ivacategid);
        $stmt->bindParam(':tipclientid', $this->tipclientid);
        $stmt->bindParam(':discount', $this->discount);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':active', $this->active);
        
     
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    public function getClient($data) {
        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE id = " . $cliid;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->id = $row['id'];
            $this->first_name= $row['first_name'];
            $this->last_name= $row['last_name'];
            $this->dni = $row['dni'];
            $this->address= $row['addrees'];
            $this->purchase_margin= $row['purchase_margin'];
            $this->ivacategid = $row['ivacategid'];
            $this->tipclientid = $row['tipclientid'];
            $this->discount = $row['discount'];
            $this->phone = $row['phone'];
            $this->active = $row['active'];

            return true;
        }

         // update a Employee record
    public function update(){
    
        // if fullname needs to be updated
        $first_name_set=!empty($this->firs) ? ", first_name = :first_name" : "";
    
        // if no posted fullname, do not update the fullname
        $query = "UPDATE " . $this->table_name . "
                SET
                    first_name = :first_name,
                    last_name = :last_name,
                    dni = :dni,
                    address = :address,
                    purchase_margin = :purchase_margin,
                    phone = :phone,
                    ivacategid = :ivacategid,
                    tipclientid = :tipclientid,
                    discount = :discount,
                    active = :active,
                    {$first_name_set}
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));

    
        // bind the values from the form
        $stmt->bindParam(':first_name', $this->first_name);

    

    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }





        function clientExists(){
 
            // query to check if email exists
            $query = "SELECT *
                    FROM " . $this->table_name . "
                    WHERE first_name= :first_name";
         
            // prepare the query
            $stmt = $this->conn->prepare( $query );
         
            // sanitize
            $this->first_name=htmlspecialchars(strip_tags($this->first_name));
    
         
            // bind given email value
            $stmt->bindParam(":first_name", $this->first_name);
    
         
            // execute the query
            $stmt->execute();
         
            // get number of rows
            $num = $stmt->rowCount();
         
            // if email exists, assign values to object properties for easy access and use for php sessions
            if($num>0){
         
                // get record details / values
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
         
                // assign values to object properties
                $this->id = $row['id'];
                $this->first_name= $row['first_name'];
                $this->last_name= $row['last_name'];
                $this->dni = $row['dni'];
                $this->address= $row['addrees'];
                $this->purchase_margin= $row['purchase_margin'];
                $this->ivacategid = $row['ivacategid'];
                $this->tipclientid = $row['tipclientid'];
                $this->discount = $row['discount'];
                $this->phone = $row['phone'];
                $this->active = $row['active'];
         
                // return false because email exists in the database
                return true;
            }
         
            // return true if email does not exist in the database
            return false;
        }

        public function getClientAll($data) {

            $query = "SELECT *
            FROM " . $this->table_name . "
            WHERE clienteid = ". $data;
    
            // bind the values from the form
            //$stmt->bindParam(':codibarra', $this->codibarra);
    
            // prepare the query
            $stmt = $this->conn->prepare($query);
    
            // sanitize
            $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));
    
            // bind the values from the form
            $stmt->bindParam(':data', $this->data);
    
            // execute the query
            if($stmt->execute()){
    
                $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // assign values to object properties
                $this->id = $row['id'];
                $this->first_name= $row['first_name'];
                $this->last_name= $row['last_name'];
                $this->dni = $row['dni'];
                $this->address= $row['addrees'];
                $this->purchase_margin= $row['purchase_margin'];
                $this->ivacategid = $row['ivacategid'];
                $this->tipclientid = $row['tipclientid'];
                $this->discount = $row['discount'];
                $this->phone = $row['phone'];
                $this->active = $row['active'];

     
                return $row;
            }    
        }
    }
    
}

?>
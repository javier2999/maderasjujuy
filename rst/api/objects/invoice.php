<?php
// 'user' object
class Invoice{
 
    // database connection and table name
    private $conn;
    private $table_name = "inv";
    
    private $table2_name = "detinv";

    public $date;
    public $subtotal;
    public $discount;
    public $total;
    public $totalpaid;
    public $rest;
    public $cliid;
    public $tipinvid;
    public $cajid;
    public $investid;
    public $date_delivery;
    public $seniaid;
    public $terminal;
    public $number;
    public $active;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
 
// create new user record
function createInvoice($detail){
 
    // insert query
    $query = "INSERT INTO " . $this->table_name . "
            SET
                subtotal = :subtotal,
                descuento = :descuento,
                total = :total,
                clienteid = :clienteid,
                tipocomprobanteid = :tipocomprobanteid";

 
    // prepare the query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->subtotal=htmlspecialchars(strip_tags($this->subtotal));
    $this->descuento=htmlspecialchars(strip_tags($this->descuento));
    $this->total=htmlspecialchars(strip_tags($this->total));
    $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));
    $this->tipocomprobanteid=htmlspecialchars(strip_tags($this->tipocomprobanteid));
    //$this->facturaid=htmlspecialchars(strip_tags($this->$data));
 
    // bind the values
    $stmt->bindParam(':subtotal', $this->subtotal);
    $stmt->bindParam(':descuento', $this->descuento);
    $stmt->bindParam(':total', $this->total);
    $stmt->bindParam(':clienteid', $this->clienteid);
    $stmt->bindParam(':tipocomprobanteid', $this->tipocomprobanteid);
    //$stmt->bindParam(':facturaid', $this->facturaid);
 
    // execute the query, also check if query was successful
    if($stmt->execute()){
        //$safa = $this->conn
        $facturaid = $this->conn->lastInsertId(); 
        $finish = false;
        foreach($detail as $objdetail)
        {
            $this->cantidad = $objdetail->cantidad;
            $this->precio = $objdetail->precio;
            $this->productoid = $objdetail->productoid;
            //$this->movimientoid = $objdetail->movimientoid;
            $this->facturaid = $facturaid;

            $queryDetail = "INSERT INTO " . $this->table2_name . "
            SET
                precio = :precio,
                productoid = :productoid,
                cantidad = :cantidad,
                facturaid = :facturaid";
                
            $stmt2 = $this->conn->prepare($queryDetail);

            $this->precio=htmlspecialchars(strip_tags($this->precio));
            $this->productoid=htmlspecialchars(strip_tags($this->productoid));
            $this->cantidad=htmlspecialchars(strip_tags($this->cantidad));
            //$this->movimientoid=htmlspecialchars(strip_tags($this->movimientoid));
            $this->facturaid=htmlspecialchars(strip_tags($facturaid));
            
            $stmt2->bindParam(':precio', $this->precio);
            $stmt2->bindParam(':productoid', $this->productoid);
            $stmt2->bindParam(':cantidad', $this->cantidad);
            //$stmt2->bindParam(':movimientoid',$this->movimientoid);
            $stmt2->bindParam(':facturaid', $facturaid);

            if($stmt2->execute()){
                $finish =  true;
            }
        }
        if($finish = true)
        {
            return true;
        }
        return false;
    }
 
    return false;
    }

    public function getInvoices($data) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE clienteid = ". $data;

        // bind the values from the form
        //$stmt->bindParam(':codibarra', $this->codibarra);

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));

        // bind the values from the form
        $stmt->bindParam(':data', $this->data);

        // execute the query
        if($stmt->execute()){

            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // assign values to object properties
            /**$this->id = $row['Id'];
            $this->CodiBarra = $row['CodiBarra'];
            $this->Descripcion = $row['Descripcion'];
            $this->PrecioCosto = $row['PrecioCosto'];
            $this->PrecioVenta = $row['PrecioVenta'];
            $this->Stock = $row['Stock'];*/
 
            return $row;
        }    
        return false;
    }

}
<?php
// 'product' object
class UnidadesProducto{
 
    // database connection and table name
    private $conn;
    private $table_name = "tipprod";
 
    // object properties
    public $id;
    public $descrip;
    public $number;
    public $active;
 
    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    public function getUnidProdAll() {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE active = 1";

        // bind the values from the form
        //$stmt->bindParam(':codbar', $this->codbar);

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            $row = $stmt->fetchAll(PDO::FETCH_ASSOC); 
            return $row;
        }    
        return false;
    }

    function create(){
 
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    descrip = :descrip,
                    number = :number,
                    active = :active";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->number=htmlspecialchars(strip_tags($this->number));
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));
        $this->active=htmlspecialchars(strip_tags($this->active));
     
        // bind the values
        $stmt->bindParam(':descrip', $this->descrip);
        $stmt->bindParam(':number', $this->number);
        $stmt->bindParam(':active', $this->active);
        
     
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function ifExist(){
 
        // query to check if email exists
        $query = "SELECT *
                FROM " . $this->table_name . "
                WHERE descrip = :descrip
                LIMIT 0,1";
     
        // prepare the query
        $stmt = $this->conn->prepare( $query );
     
        // sanitize
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));
     
        // bind given email value
        $stmt->bindParam(":descrip", $this->descrip);
     
        // execute the query
        $stmt->execute();
     
        // get number of rows
        $num = $stmt->rowCount();
     
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
     
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
            // assign values to object properties
            $this->id = $row['id'];
            $this->descrip = $row['descrip'];
            $this->cost_price = $row['number'];
            $this->active = $row['active'];
     
            // return false because email exists in the database
            return true;
        }
     
        // return true if email does not exist in the database
        return false;
    }

    public function update(){
     
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    descrip = :descrip,
                    number = :number,
                    active = :active
                WHERE id = :id";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));
        $this->number=htmlspecialchars(strip_tags($this->number));
        $this->active=htmlspecialchars(strip_tags($this->active));
     
        // bind the values from the form
        $stmt->bindParam(':descrip', $this->descrip);
        $stmt->bindParam(':number', $this->number);
        $stmt->bindParam(':active', $this->active);
     
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    public function updateState(){
     
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
     
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
     
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    public function delete(){
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
    
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }    

}

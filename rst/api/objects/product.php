<?php
// 'product' object
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "prod";
 
    // object properties
    public $id;
    public $codbar;
    public $descrip;
    public $cost_price;
    public $sell_price;
    public $stock;
    public $unityid;
    public $tipprodid;
    public $active;
 
    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    public function getProduct($data) {

        $query = "SELECT * FROM " . $this->table_name . "WHERE codbar = " . $data;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->codbar=htmlspecialchars(strip_tags($this->codbar));


        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->id = $row['id'];
            $this->codbar = $row['codbar'];
            $this->descrip = $row['descrip'];
            $this->cost_price = $row['cost_price'];
            $this->sell_price = $row['sell_price'];
            $this->stock = $row['stock'];
            $this->active = $row['active'];
 
            return true;
        }    
        return false;
    }

    public function getProductDescrip($data) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE descrip LIKE('%" . $data . "%')";

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->codbar=htmlspecialchars(strip_tags($this->codbar));

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $row;
        }    
        return false;
    }

    public function getProductAll() {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE active = 1";

        // bind the values from the form
        //$stmt->bindParam(':codbar', $this->codbar);

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // assign values to object properties
            /**$this->id = $row['id'];
            $this->codbar = $row['codbar'];
            $this->descrip = $row['descrip'];
            $this->cost_price = $row['cost_price'];
            $this->sell_price = $row['sell_price'];
            $this->stock = $row['stock'];*/
 
            return $row;
        }    
        return false;
    }

    function create(){
 
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    codbar = :codbar,
                    descrip = :descrip,
                    cost_price = :cost_price,
                    sell_price = :sell_price,
                    unityid = :unityid,
                    tipprodid = :tipprodid,
                    stock = :stock,
                    active = :active";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->codbar=htmlspecialchars(strip_tags($this->codbar));
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));
        $this->cost_price=htmlspecialchars(strip_tags($this->cost_price));
        $this->sell_price=htmlspecialchars(strip_tags($this->sell_price));
        $this->unityid=htmlspecialchars(strip_tags($this->unityid));
        $this->tipprodid=htmlspecialchars(strip_tags($this->tipprodid));
        $this->stock=htmlspecialchars(strip_tags($this->stock));
        $this->active=htmlspecialchars(strip_tags($this->active));
     
        // bind the values
        $stmt->bindParam(':codbar', $this->codbar);
        $stmt->bindParam(':descrip', $this->descrip);
        $stmt->bindParam(':cost_price', $this->cost_price);
        $stmt->bindParam(':unityid', $this->unityid);
        $stmt->bindParam(':tipprodid', $this->tipprodid);
        $stmt->bindParam(':sell_price', $this->sell_price);
        $stmt->bindParam(':stock', $this->stock);
        $stmt->bindParam(':active', $this->active);
        
     
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function productExists(){
 
        // query to check if email exists
        $query = "SELECT *
                FROM " . $this->table_name . "
                WHERE codbar = :codbar
                LIMIT 0,1";
     
        // prepare the query
        $stmt = $this->conn->prepare( $query );
     
        // sanitize
        $this->codbar=htmlspecialchars(strip_tags($this->codbar));
     
        // bind given email value
        $stmt->bindParam(":codbar", $this->codbar);
     
        // execute the query
        $stmt->execute();
     
        // get number of rows
        $num = $stmt->rowCount();
     
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
     
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
            // assign values to object properties
            $this->id = $row['id'];
            $this->codbar = $row['codbar'];
            $this->descrip = $row['descrip'];
            $this->cost_price = $row['cost_price'];
            $this->sell_price = $row['sell_price'];
            $this->stock = $row['stock'];
            $this->active = $row['active'];
     
            // return false because email exists in the database
            return true;
        }
     
        // return true if email does not exist in the database
        return false;
    }

    public function getStock($data,$array) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE id = :".$data;

        // bind the values from the form
        //$stmt->bindParam(':codbar', $this->codbar);

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));

        // bind the values from the form
        $stmt->bindParam(':clienteid', $this->data);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $array = $row;
 
            return $array;
        }    
        return false;
    }
    public function update(){
     
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    codbar = :codbar,
                    descrip = :descrip,
                    cost_price = :cost_price,
                    sell_price = :sell_price,
                    stock = :stock
                WHERE id = :id";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->codbar=htmlspecialchars(strip_tags($this->codbar));
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));
        $this->cost_price=htmlspecialchars(strip_tags($this->cost_price));
        $this->sell_price=htmlspecialchars(strip_tags($this->sell_price));
        $this->stock=htmlspecialchars(strip_tags($this->stock));
     
        // bind the values from the form
        $stmt->bindParam(':codbar', $this->codbar);
        $stmt->bindParam(':descrip', $this->descrip);
        $stmt->bindParam(':cost_price', $this->cost_price);
        $stmt->bindParam(':sell_price', $this->sell_price);
        $stmt->bindParam(':stock', $this->stock);
     
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    public function updatestate(){
     
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
     
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
     
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

<<<<<<< HEAD
}
=======
    public function deleteProduct(){
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
    
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }    

}
>>>>>>> 44c57e5d222dc2e258bf779484afe3814195a4e6

<?php
// 'user' object
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "usr";
 
    // object properties
    public $id;
    public $nick;
    public $email;
    public $groupid;
    public $password;
    public $created_at;
    public $date;
    public $jwt;
    public $last_session;
    public $active;
 
    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
 
    // create new user record
    function create(){
    
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
<<<<<<< HEAD
                    nick = :nick,
                    groupid = :groupid,
                    email = :email,
<<<<<<< HEAD
<<<<<<< HEAD
                    password = :password,
=======
                    password = :password
>>>>>>> e77abf845120e4f52b0efa0bc2085f87681b0e3d
=======
                    password = :password,
>>>>>>> 08532448cca6ada61855acf007e8405ff02e5c66
=======
                    first_name = :first_name,
                    last_name = :last_name,
                    email = :email,
<<<<<<< HEAD
<<<<<<< HEAD
                    password = :password,
<<<<<<< HEAD
=======
                    password = :password
>>>>>>> e77abf845120e4f52b0efa0bc2085f87681b0e3d
=======
                    password = :password,
>>>>>>> 08532448cca6ada61855acf007e8405ff02e5c66
=======
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
>>>>>>> 44c57e5d222dc2e258bf779484afe3814195a4e6
                    active = 1";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
<<<<<<< HEAD
        $this->nick=htmlspecialchars(strip_tags($this->nick));
        $this->groupid=htmlspecialchars(strip_tags($this->groupid));
=======
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->password=htmlspecialchars(strip_tags($this->password));
    
        // bind the values
<<<<<<< HEAD
        $stmt->bindParam(':nick', $this->nick);
        $stmt->bindParam(':groupid', $this->groupid);
=======
        $stmt->bindParam(':first_name', $this->first_name);
        $stmt->bindParam(':last_name', $this->last_name);
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
        $stmt->bindParam(':email', $this->email);
    
        // hash the password before saving to database
        $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
        $stmt->bindParam(':password', $password_hash);
    
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
 
    // check if given email exist in the database
    function emailExists(){
    
        // query to check if email exists
<<<<<<< HEAD
        $query = "SELECT id, nick, created_at, jwt, password,groupid
                FROM " . $this->table_name . "
                WHERE email = ? AND active!=0
=======
        $query = "SELECT id, first_name, last_name, password, bussinesId
                FROM " . $this->table_name . "
                WHERE email = ?
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
                LIMIT 0,1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind given email value
        $stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->id = $row['id'];
<<<<<<< HEAD
            $this->nick = $row['nick'];
            $this->groupid = $row['groupid'];
            $this->jwt = $row['jwt'];
            $this->password = $row['password'];
            $this->created_at = $row['created_at'];
=======
            $this->first_name = $row['first_name'];
            $this->last_name = $row['last_name'];
            $this->password = $row['password'];
            $this->bussinesId = $row['bussinesId'];
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
    
            // return true because email exists in the database
            return true;
        }
    
        // return false if email does not exist in the database
        return false;
    }
 
    // update a user record
    public function update(){
    
        // if password needs to be updated
        $password_set=!empty($this->password) ? ", password = :password" : "";
    
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
<<<<<<< HEAD
                    nick = :nick,
                    groupid = :groupid,
=======
                    first_name = :first_name,
                    last_name = :last_name,
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
                    email = :email
                    {$password_set}
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
<<<<<<< HEAD
        $this->nick=htmlspecialchars(strip_tags($this->nick));
        $this->groupid=htmlspecialchars(strip_tags($this->groupid));
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind the values from the form
        $stmt->bindParam(':nick', $this->nick);
        $stmt->bindParam(':groupid', $this->groupid);
=======
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind the values from the form
        $stmt->bindParam(':first_name', $this->first_name);
        $stmt->bindParam(':last_name', $this->last_name);
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
        $stmt->bindParam(':email', $this->email);
    
        // hash the password before saving to database
        if(!empty($this->password)){
            $this->password=htmlspecialchars(strip_tags($this->password));
            $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
            $stmt->bindParam(':password', $password_hash);
        }
    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    public function deleteUser(){
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
    
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    public function getAllUsers() {
		
<<<<<<< HEAD
        $users = array();
        
        $query = "SELECT a.id,a.nick, a.email, a.created_at, IF(ISNULL(b.emplid),0,b.emplid) as emplid, IF(ISNULL(c.groupname),'No Asignado', c.groupname) as groupname, IF(a.active=1,'Habilitado','Deshabilitado') as active  FROM usr a LEFT JOIN empluser b ON a.id = b.usrid
        LEFT JOIN groups c ON a.groupid = c.id";

    
        $stmt = $this->conn->prepare( $query );

        $stmt->execute();
    
        $num = $stmt->rowCount();
    
        if($num>0){

            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $users;
        }
=======
		$users = array();

        // query to check if email exists
        $query = "SELECT *
                FROM " . $this->table_name." WHERE active = 1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // bind given email value
        //$stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            //$row = $stmt->fetch(PDO::FETCH_ASSOC);
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // assign values to object properties
           /* $this->id = $row['id'];
            $this->first_name = $row['first_name'];
            $this->last_name = $row['last_name'];
            $this->dni = $row['dni'];
            $this->phone = $row['phone'];
            $this->password = $row['password'];
            $this->bussinesId = $row['bussinesId'];*/
    
            // return true because email exists in the database
            return $users;
        }
    
        // return false if email does not exist in the database
>>>>>>> 38c12e70d028f0f4a5900ab732169b7ffdcaf401
        return false;
    }
}
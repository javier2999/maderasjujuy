<?php  
class Provedor{
private $conn;
private $table_name = "provider";

public $id;
public $company_name;
public $cuit;
public $ivacategid;
public $phone;
public $active;



// constructor
    public function __construct($db){
    $this->conn = $db;
    }

    function create(){
 
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    company_name = :company_name,
                    cuit = :cuit,
                    ivacategid = :ivacategid,
                    phone = :phone,
                    active = 1";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->company_name=htmlspecialchars(strip_tags($this->company_name));
     
        // bind the values
        $stmt->bindParam(':company_name', $this->company_name);
        $stmt->bindParam(':cuit', $this->cuit);
        $stmt->bindParam(':ivacategid', $this->ivacategid);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':active', $this->active);
        
     
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }



     // update a provider record
     public function update(){
    
        // if fullname needs to be updated
        $company_name_set=!empty($this->company_name) ? ", company_name = :company_name" : "";
    
        // if no posted fullname, do not update the fullname
        $query = "UPDATE " . $this->table_name . "
                SET
                    company_name = :dncompany_namei,
                    cuit = :cuit,
                    ivacategid = :ivacategid,
                    phone = :phone,
                    active = :active,
                    {$company_name_set}
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->comapny_name=htmlspecialchars(strip_tags($this->company_name));

    
        // bind the values from the form
        $stmt->bindParam(':company_name', $this->company_name);

    

    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
    
    public function getProvider($data) {
        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE company_name= " . $data;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->id = $row['id'];
            $this->company_name= $row['company_name'];
            $this->ivacategid = $row['ivacategid'];
            $this->cuit = $row['cuit'];
            $this->phone = $row['phone'];
            $this->active = $row['active'];

            return true;
        }
        public function getProviderAll($data) {

            $query = "SELECT *
            FROM " . $this->table_name . "
            WHERE clienteid = ". $data;
    
            // bind the values from the form
            //$stmt->bindParam(':codibarra', $this->codibarra);
    
            // prepare the query
            $stmt = $this->conn->prepare($query);
    
            // sanitize
            $this->providerid=htmlspecialchars(strip_tags($this->pdroviderid));
    
            // bind the values from the form
            $stmt->bindParam(':data', $this->data);
    
            // execute the query
            if($stmt->execute()){
    
                $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // assign values to object properties
                $this->id = $row['id'];
                $this->company_name= $row['company_name'];
                $this->ivacategid = $row['ivacategid'];
                $this->cuit = $row['cuit'];
                $this->phone = $row['phone'];
                $this->active = $row['active'];

     
                return $row;
            }    
        }
    }
    
}

?>
<?php
// 'user' object
class Groups{
 
    // database connection and table name
    private $conn;
    private $table_name = "groups";
 
    // object properties
    public $id;
    public $descrip;
    public $active;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
 
    // create new user record
    function create(){
    
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    descrip = :descrip,
                    active = :1,";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));

        // bind the values
        $stmt->bindParam(':descrip', $this->descipt);
        $stmt->bindParam(':active', $this->active);

    

    
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }



    public function delete(){
        // if no posted fullname, do not update the fullname
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
    
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
 
    // check if given email exist in the database
    function groupsExists(){
    
        // query to check if email exists
        $query = "SELECT id, descrip, active
                FROM " . $this->table_name . "
                WHERE descrip = ?
                LIMIT 0,17";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    

   
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->id = $row['id'];
            $this->descrip = $row['descrip'];
            $this->active = $row['active'];
 
            // return true because email exists in the database
            return true;
        }
    
        // return false if email does not exist in the database
        return false;
    }
 
    // update a user record
    public function update(){
    
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    descrip = :descrip,
                    active = :active,

                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->descrip=htmlspecialchars(strip_tags($this->descrip));

        // bind the values from the form
        $stmt->bindParam(':descrip', $this->descrip);
        $stmt->bindParam(':active', $this->active);

    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    public function getAllGroups() {
		
		$groups = array();

        // query to check if email exists
        $query = "SELECT id, descript, active
                FROM " . $this->table_name;
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // bind given email value
        //$stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            $groups = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $groups;
        }
    
        // return false if email does not exist in the database
        return false;
    }
}
?>
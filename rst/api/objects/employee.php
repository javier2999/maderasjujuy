<?php
// 'user' object
class Employee{
 
    // database connection and table name
    private $conn;
    private $table_name = "employee";
 
    // object properties
    public $id;
    public $fullname;
    public $dni;
    public $address;
    public $salary;
    public $phone;
    public $date;
    public $active;
 
    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

 
    // create new Employee record

    function create(){
 
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    fullname = :fullname,
                    dni = :dni,
                    address = :address,
                    salary = :salary,
                    phone = :phone,
                    date = :date,
                    active = 1";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->fullname=htmlspecialchars(strip_tags($this->fullname));
     
        // bind the values
        $stmt->bindParam(':fullname', $this->fullname);
        $stmt->bindParam(':dni', $this->dni);
        $stmt->bindParam(':address', $this->address);
        $stmt->bindParam(':salary', $this->salary);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':date', $this->date);
        $stmt->bindParam(':active', $this->active);
        
     
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function employeeExists(){
 
        // query to check if email exists
        $query = "SELECT *
                FROM " . $this->table_name . "
                WHERE fullname = :fullname";
     
        // prepare the query
        $stmt = $this->conn->prepare( $query );
     
        // sanitize
        $this->fullname=htmlspecialchars(strip_tags($this->fullname));

     
        // bind given email value
        $stmt->bindParam(":fullname", $this->fullname);

     
        // execute the query
        $stmt->execute();
     
        // get number of rows
        $num = $stmt->rowCount();
     
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
     
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
            // assign values to object properties
            $this->id = $row['id'];
            $this->fullname = $row['fullname'];
            $this->dni = $row['dni'];
            $this->address = $row['address'];
            $this->Phone = $row['Telefono'];
            $this->salary = $row['salary'];
            $this->date = $row['date'];
            $this->active = $row['active'];
     
            // return false because email exists in the database
            return true;
        }
     
        // return true if email does not exist in the database
        return false;
    }



    public function getemployee($data) {
        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE fullname = " . $data;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->id = $row['id'];
            $this->fullname = $row['fullname'];
            $this->dni = $row['dni'];
            $this->address = $row['address'];
            $this->Phone = $row['Telefono'];
            $this->salary = $row['salary'];
            $this->date = $row['date'];
            $this->active = $row['active'];


            return true;
        }
    
        
        return false;
    }
 

    // update a Employee record
    public function update(){
    
        // if fullname needs to be updated
        $fullname_set=!empty($this->fullname) ? ", fullname = :fullname" : "";
    
        // if no posted fullname, do not update the fullname
        $query = "UPDATE " . $this->table_name . "
                SET
                    dni = :dni,
                    address = :address,
                    salary = :salary,
                    phone = :phone,
                    date = :date,
                    {$fullname_set}
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->fullname=htmlspecialchars(strip_tags($this->fullname));

    
        // bind the values from the form
        $stmt->bindParam(':fullname', $this->fullname);

    

    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    public function delete(){
        // if no posted fullname, do not update the fullname
        $query = "UPDATE " . $this->table_name . "
                SET
                    active = :active
                WHERE id = :id";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->active=htmlspecialchars(strip_tags($this->active));
    
        // bind the values from the form
        $stmt->bindParam(':active', $this->active);
    
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    public function getallemplyee() {
		
		$employee = array();

        // query to check if fullname exists
        $query = "SELECT *
                FROM " . $this->table_name." WHERE active = 1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // bind given email value
        //$stmt->bindParam(1, $this->fullname);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if fullname exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            //$row = $stmt->fetch(PDO::FETCH_ASSOC);
            $employee = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->id = $row['id'];
            $this->fullname = $row['fullname'];
            $this->dni = $row['dni'];
            $this->address = $row['address'];
            $this->Phone = $row['Telefono'];
            $this->salary = $row['salary'];
            $this->date = $row['date'];
            $this->active = $row['active'];*/
    
            // return true because email exists in the database
            return $employee;
        }
    
        // return false if email does not exist in the database
        return false;
    }
}
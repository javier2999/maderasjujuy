<?php  
class Provedor{
private $conn;
private $table_name = "prvd";

public $id;
public $company_name;
public $cuit;
public $phone;
public $active;

// constructor
    public function __construct($db){
    $this->conn = $db;
    }

    public function getProvedor($data) {
        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE NombreEmp = " . $data;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->Id = $row['Id'];
            $this->NombreEmp= $row['NombreEmpresa'];
            $this->CategoriaIva = $row['categoriaIva'];
            $this->Cuit = $row['Cuit'];
            $this->Phone = $row['Telefono'];

            return true;
        }
        public function getProvedorAll($data) {

            $query = "SELECT *
            FROM " . $this->table_name . "
            WHERE clienteid = ". $data;
    
            // bind the values from the form
            //$stmt->bindParam(':codibarra', $this->codibarra);
    
            // prepare the query
            $stmt = $this->conn->prepare($query);
    
            // sanitize
            $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));
    
            // bind the values from the form
            $stmt->bindParam(':data', $this->data);
    
            // execute the query
            if($stmt->execute()){
    
                $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // assign values to object properties
                /**$this->Id = $row['Id'];
                   $this->NombreEmp= $row['NombreEmpresa'];
                   $this->CategoriaIva = $row['categoriaIva'];
                   $this->Cuit = $row['Cuit'];
                   $this->Phone = $row['Telefono'];

     
                return $row;
            }    
        }
    }
    
}

?>
<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/provider.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate Provedor object
$provider = new provider($db);
 
$data  =file_get_contents("php://input");
// get posted data
$data = json_decode(file_get_contents("php://input"));
$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $company_name => $valor) {
	if(substr($company_name,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
 
// if jwt is not empty
if($jwt2){
 
    // if decode succeed, show product details
    try {
 
        // decode jwt
        $decoded = JWT::decode($jwt2, $key, array('HS256'));

        // set user property values
		
		$provider->providerid = $data->providerid;

		
			$arrayprovider = $provider->getproviderAll($data->providerid);
			// update the product record
			if($arrayprovider){
				// we need to re-generate jwt because product details might be different
				$token = array(
					"provider" => $arrayprovider
				);
				
				$jwt = JWT::encode($token, $key);
				
				// set response code
				http_response_code(200);
				
				// response in json format
				echo json_encode(array("provider" => $arrayprovider));
			}
			
			// message if unable to update product
			else{
				// set response code
				http_response_code(401);
			
				// show error message
				echo json_encode(array("provider" => array()));
			}
		}
		
				
			
			
			
		
    }
 
   // if decode fails, it means jwt is invalid
	catch (Exception $e){
	 
		// set response code
		http_response_code(401);
	 
		// show error message
		echo json_encode(array(
			"message" => "Access denied.",
			"error" => $e->getMessage(),
			"safa"=> $jwt2
		));
	}
}
 
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the product access denied
    echo json_encode(array("message" => $jwt));
}
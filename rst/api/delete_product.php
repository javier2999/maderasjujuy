<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/product.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$product = new Product($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
////$jwt=isset($data->jwt) ? $data->jwt : "";
 
// if jwt is not empty
////if($jwt){
 
    // if decode succeed, show product details
    try {
 
        // decode jwt
        ////$decoded = JWT::decode($jwt, $key, array('HS256'));
 
		// set product property valuesm
		$product->active = $data->active;
		//$product->id = $decoded->data->id;
		$product->id = $data->id;
		 
		// update the product record
		if($product->deleteProduct()){
			// we need to re-generate jwt because product details might be different
			$token = array(
			   "data" => array(
                   "id" => $product->id,
                   "active" => $product->active
			   )
			);
			////$jwt = JWT::encode($token, $key);
			 
			// set response code
			http_response_code(200);
			 
			// response in json format
			echo json_encode(
					array(
                        "message" => $data,
                        "status" => true
						////"jwt" => $jwt
					)
				);
		}
		 
		// message if unable to update product
		else{
			// set response code
			http_response_code(401);
		 
			// show error message
			echo json_encode(array("message" => $data));
		}
    }
 
    // if decode fails, it means jwt is invalid
	catch (Exception $e){
	 
		// set response code
		http_response_code(401);
	 
		// show error message
		echo json_encode(array(
			"message" => "Access denied.",
			"error" => $e->getMessage()
		));
	}
////}
 
// show error message if jwt is empty
////else{
 
    // set response code
////    http_response_code(401);
 
    // tell the product access denied
////    echo json_encode(array("message" => "Access denied."));
////}
?>
<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/Client.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate employee object
$employee = new employee($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set emplyee property values
$emplyee-> id = $data->id;
$emplyee-> fullname = $data->fullname;
$emplyee-> dni = $data->dni;
$emplyee-> address = $data->address;
$emplyee-> phone = $data->phone;
$emplyee-> salary= $data->salary;
$emplyee-> date = $data->date;
$emplyee-> active = $data->active;


$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $fullname => $valor) {
	if(substr($fullname,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
if($jwt2){
    if(!$employee->employee_Exists())
    {
        if(
            !empty($employee->fullname) &&
            !empty($employee->dni) &&
            !empty($employee->address) &&
            !empty($employee->Phone) &&
            !empty($employee->salary) &&
            !empty($employee->date) &&
            !empty($employee->active) &&
            $Client->create()
        ){
            $token = array(
                "employeeid" => 1,
                "respuesta" => "safa",
                "message" => "Employee was created."
            );
        
            http_response_code(200);
            echo json_encode(array($token));
        }
        else{
            $token = array(
                "employeeId" => 1,
                "respuesta" => "safa",
                "message" => "Unable to create Employee."
            );
            http_response_code(400);
            echo json_encode(array($token));
        }
    }
    else
    {
        $token = array(
            "employeeId" => 1,
            "respuesta" => "safa",
            "message" => "Emplyee existente"
        );
        http_response_code(400);
        echo json_encode(array($token));
    }
}
else{
        // set response code
        http_response_code(401);
 
        // tell the product access denied
        echo json_encode(array("message" => $jwt2));
}

?>
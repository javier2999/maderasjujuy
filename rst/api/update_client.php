<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 

// PRUEBA MODIFICACION SANTIAGO

// PRUEBA MODIFICACION SANTIAGO
// PRUEBA MODIFICACION SANTIAGO
// PRUEBA MODIFICACION SANTIAGO

// PRUEBA MODIFICACION SANTIAGO
// PRUEBA MODIFICACION SANTIAGO


// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/client.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate Client object
$client = new client($db);
 
$data  =file_get_contents("php://input");
// get posted data
$data = json_decode(file_get_contents("php://input"));
$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $first_name => $valor) {
	if(substr($first_name,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
 
// if jwt is not empty
if($jwt2){
 
    // if decode succeed, show product details
    try {
 
        // decode jwt
        $decoded = JWT::decode($jwt2, $key, array('HS256'));
 
		// set product property values
        $client-> id = $data->id;
		$client-> first_name = $data->first_name;
		$client-> last_name = $data->last_name;
		$client-> dni = $data->dni;
		$client-> purchase_margin = $data->purchase_margin;
		$client-> ivacategid = $data->ivacategid;
		$client-> address = $data->address;
		$client-> tipclientid = $data->tipclientid;
		$client-> phone = $data->phone;
		$client-> discount = $data->discount;
		$client-> active = $data->active;
		 
		// update the Client record
		if($client->update()){
			// we need to re-generate jwt because Client details might be different
			$token = array(
				   "id" => $client->id,
				   "first_name" => $client->first_name,
				   "last_name" => $client->last_name,
				   "dni" => $client->dni,
				   "purchase_margin" => $client->purchase_margin,
				   "ivacategid" => $client->ivacategid,
				   "phone" => $client->phone,
				   "address" => $client->address,
                   "tipclientid" => $client->tipclientid,
				   "discount" => $client->discount,
				   "active" => $client->active
       
			   );
			$jwt2 = JWT::encode($token, $key);
			 
			// set response code
			http_response_code(200);
			 
			// response in json format
			echo json_encode(
					array(
						"message" => "Client was updated.",
						"jwt" => $jwt
					)
				);
		}
		 
		// message if unable to update Client
		else{
			// set response code
			http_response_code(401);
		 
			// show error message
			echo json_encode(array("message" => "Unable to update Client."));
		}
    }
 
    // if decode fails, it means jwt is invalid
	catch (Exception $e){
	 
		// set response code
		http_response_code(401);
	 
		// show error message
		echo json_encode(array(
			"message" => "Access denied.",
			"error" => $e->getMessage()
		));
	}
}
 
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the product access denied
    echo json_encode(array("message" => "Access denied."));
}
?>
<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/client.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate Client object
$client = new client($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set client property values
$client-> id = $data->id;
$client-> first_name = $data->first_name;
$client-> last_name = $data->last_name;
$client-> dni = $data->dni;
$client-> purchase_margin = $data->purchase_margin;
$client-> ivacategid = $data->ivacategid;
$client-> address = $data->address;
$client-> tipclientid = $data->tipclientid;
$client-> phone = $data->phone;
$client-> discount = $data->discount;
$client-> active = $data->active;


$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $first_name => $valor) {
	if(substr($first_name,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
if($jwt2){
    if(!$Client->clientExists())
    {
        if(
            !empty($client->first_name) &&
            !empty($client->last_name) &&
            !empty($client->dni) &&
            !empty($client->addrees) &&
            !empty($client->purchase_margin) &&
            !empty($client->ivacategid) &&
            !empty($client->discount) &&
            !empty($client->phone) &&
            !empty($client->tipclientid) &&
            !empty($client->active) &&
            $client->create()
        ){
            $token = array(
                "status" => true,
                "message" => "Client was created."
            );
        
            http_response_code(200);
            echo json_encode(array($token));
        }
        else{
            $token = array(
                "status" => false,
                "message" => "Unable to create Client."
            );
            http_response_code(400);
            echo json_encode(array($token));
        }
    }
    else
    {
        $token = array(
            "status" => false,
            "message" => "Cliente existente"
        );
        http_response_code(400);
        echo json_encode(array($token));
    }
}
else{
        // set response code
        http_response_code(401);
 
        // tell the product access denied
        echo json_encode(array("message" => $jwt2));
}

?>